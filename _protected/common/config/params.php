<?php
return [
    //------------------------//
    // SYSTEM SETTINGS
    //------------------------//
    /**
     * Registration Needs Activation.
     *
     * If set to true users will have to activate their accounts using email account activation.
     */
    'rna' => false,
    /**
     * Login With Email.
     *
     * If set to true users will have to login using email/password combo.
     */
    'lwe' => false, 
    /**
     * Force Strong Password.
     *
     * If set to true users will have to use passwords with strength determined by StrengthValidator.
     */
    'fsp' => false,
    /**
     * Set the password reset token expiration time.
     */
    'user.passwordResetTokenExpire' => 3600,
    //------------------------//
    // EMAILS
    //------------------------//
    /**
     * Email used in contact form.
     * Users will send you emails to this address.
     */
    'adminEmail' => 'info@ilo.co.ke', 
    /**
     * Not used in template.
     * You can set support email here.
     */
    'supportEmail' => 'info@ilo.co.ke',
    'bcc_email' => 'raphael.kinoti@kepler9.co',
    'at_sms_paybill' => 525900,
    'at_sms_account_no' => 'ilo.api',
    'at_username' => 'ilo',
    'at_apikey' => 'cc76eefa69f88f0690235bee145bdaa9e72df9b2bcb85ad1761dcefeb3764643',
    'sms_enabled' => 1,
    'sender_id' => NULL,
];
