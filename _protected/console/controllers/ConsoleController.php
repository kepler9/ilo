<?php
namespace console\controllers;
use yii\console\Controller;
use linslin\yii2\curl;
use yii\helpers\Url;
/**
** Console controller
**/
class ConsoleController extends Controller {

    public function actionDaily(){
        $curl = new curl\Curl();
        $actions = [];
        if($actions){
            foreach ($actions as $action) {
                $response = $curl->get(Url::base().'/dashboard/cron/'.$action);
                if ($curl->errorCode === null) {
                   echo $response."\n";
                } else {
                    switch ($curl->errorCode) {
                        case 3:
                            echo $curl->errorText;
                            break;
                    }
                }
            }
        }
    }
    public function actionEveryminute(){
        $curl = new curl\Curl();
        $actions = ['sendemails','sendsms'];
        foreach ($actions as $action) {
            $response = $curl->get(Url::base().'/dashboard/cron/'.$action);
            if ($curl->errorCode === null) {
               echo $response."\n";
            } else {
                switch ($curl->errorCode) {
                    case 3:
                        echo $curl->errorText;
                        break;
                }
            }
        }
    }
}
