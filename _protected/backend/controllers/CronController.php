<?php
namespace backend\controllers;
use yii;
use backend\models\MailQueue;
use backend\models\Cron;
/**
**
**/
class CronController extends \yii\web\Controller
{
    public $cron;
    public function beforeAction($action){
        $this->cron = new Cron();
        return parent::beforeAction($action);
    }
    public function actionSendemails(){
        $this->cron->send_emails();
    }
    public function actionSendsms(){
        $this->cron->_send_sms();
    }
    public function actionEmailpreview(){
        return $this->renderPartial('../../../common/mail/email_template');
    }
}