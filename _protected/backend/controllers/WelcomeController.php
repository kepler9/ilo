<?php
 namespace backend\controllers;
 use yii;
 /**
 ** Main controller
 **/
 class WelcomeController extends MainController {
 	public function actionIndex(){
 		return $this->render('index');
 	}
 }