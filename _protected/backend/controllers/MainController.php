<?php
 namespace backend\controllers;
 use yii;
 use backend\models\Admins;
 use backend\models\Models;
 use backend\models\Users;

 /**
 * Main controller
 */

 class MainController extends \yii\web\Controller
 {
    public $user;
    public $app;
    public $admin;
    public $model;

 	public function beforeAction($action) {
 		$this->layout = 'backend';
        $this->app = Yii::$app;
        $this->model = new Models();
        if(Yii::$app->session->get('user')){
            $user = Users::findOne(Yii::$app->session->get('user')->user_id);
            if($user->role == 'admin'){
                $this->user = $user;
                $this->admin = Admins::findOne(['user_id'=>$this->user->user_id]);
                if(!$this->admin){
                    header('location:'.Yii::$app->request->hostInfo);exit;
                }
            }else{
                header('location:'.Yii::$app->request->hostInfo);exit;
            }
        }else{
            header('location:'.Yii::$app->request->hostInfo.'/dashboard/account/login');exit;
        }
 		return parent::beforeAction($action);
 	}
    public function error_msg()
    {
        Yii::$app->session->setFlash('error_msg',true);
    }

    public function clean($string)
    {
        $string = str_replace(' ', '-', $string);
        return strtolower(preg_replace('/[^A-Za-z0-9\-.]/', '-', $string));
    }
 }
