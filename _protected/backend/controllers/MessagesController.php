<?php
namespace backend\controllers;
use yii;
use backend\models\SmsQueue;
use backend\models\Drivers;
use backend\models\Merchants;
/**
 * 
 */
class MessagesController extends MainController
{
	public function actionBulk(){
		return $this->render('bulk');
	}
	public function actionSend(){
		$data = Yii::$app->request->post();
		if($data){
			if($data['recipient'] == 'drivers'){
				$drivers = Drivers::findAll(['status'=>1]);
				if($drivers){
					foreach($drivers as $key){
						$queue = new SmsQueue();
						$queue->phone_number = (string) $key->user->phone;
						$queue->message = $data['message'];
						$queue->save();
					}
				}
			}else if($data['recipient'] == 'merchants'){
				$merchants = Merchants::findAll(['status'=>1]);
				if($merchants){
					foreach($merchants as $key){
						$queue = new SmsQueue();
						$queue->phone_number = (string) $key->user->phone;
						$queue->message = $data['message'];
						$queue->save();
					}
				}
			}else if($data['recipient'] == 'phone'){
				$queue = new SmsQueue();
				$queue->phone_number = $data['phone'];
				$queue->message = $data['message'];
				$queue->save();
			}
			$this->model->msg('SMS scheduled for sending . . .');
			return $this->redirect(['bulk']);
		}
	}
}