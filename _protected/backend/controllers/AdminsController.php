<?php
namespace backend\controllers;
use Yii;
use backend\models\Admins;
use backend\models\AdminsSearch;
use backend\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Templates;
use backend\models\MailQueue;
use backend\models\Users;
/**
 * AdminsController implements the CRUD actions for Admins model.
 */
class AdminsController extends MainController {
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Admins models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AdminsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admins model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id=null) {
        return $this->render('view', [
            'model' => $this->findModel($this->admin->admin_id),
        ]);
    }

    /**
     * Creates a new Admins model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Admins();
        if(Yii::$app->request->post()){
            $data = Yii::$app->request->post();
            $user = new Users();
            $user->load(Yii::$app->request->post());
            $user->role = 'admin';
            $user->password = Yii::$app->getSecurity()->generatePasswordHash($data['password']);
            if($user->save()){
                $model->user_id = $user->user_id;
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    $template = Templates::findOne(['name'=>'admin_account']);
                    if($template){
                        $queue = new MailQueue();
                        $queue->email_to = $user->email;
                        $queue->subject = $template->subject;
                        $queue->html_body = $template->body;
                        $queue->html_body = str_replace('[NAME]', $user->first_name, $queue->html_body);
                        $queue->html_body = str_replace('[EMAIL]', $user->email, $queue->html_body);
                        $queue->html_body = str_replace('[PASSWORD]', $data['password'], $queue->html_body);
                        $queue->html_body = str_replace('[LINK]', Yii::$app->request->hostInfo.'/dashboard/account/login', $queue->html_body);
                        $queue->save();
                    }
                    $this->model->msg('Account created');
                    return $this->redirect(['view', 'id' => $model->admin_id]);
                }
            }else{
                $this->model->error_msg($this->model->get_model_errors($user));
            }
        }
        return $this->render('create', [
            'model' => $model,
            'user' => new Users(),
        ]);
    }

    /**
     * Updates an existing Admins model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    **/
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $data = Yii::$app->request->post();
        if ($model->load(Yii::$app->request->post())) {
            $model->user->load(Yii::$app->request->post());
            if($data['password']){
                $model->user->password = Yii::$app->getSecurity()->generatePasswordHash($data['password']);
            }
            if($model->user->save() && $model->save()){
                return $this->redirect(['view', 'id' => $model->admin_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'user' => $model->user,
        ]);
    }

    /**
     * Deletes an existing Admins model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    **/

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if($this->admin->level == 1 && $this->admin->level != $model->level){
            $this->findModel($id)->user->delete();
        }else{
            $this->model->error_msg('Action failed');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Admins model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admins the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/

    protected function findModel($id) {
        if (($model = Admins::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionProfile(){
        return $this->redirect(['view','id'=>$this->admin->admin_id]);
    }
}
