<?php

namespace backend\controllers;

use Yii;
use backend\models\Merchants;
use backend\models\MerchantsSearch;
use backend\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Users;
use backend\models\Templates;
use backend\models\MailQueue;

/**
 * MerchantsController implements the CRUD actions for Merchants model.
 */
class MerchantsController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Merchants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MerchantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Merchants model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Merchants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Merchants();

        if(Yii::$app->request->post()){
            $data = Yii::$app->request->post();
            $user = new Users();
            $user->load(Yii::$app->request->post());
            $user->role = 'merchant';
            $user->password = Yii::$app->getSecurity()->generatePasswordHash($data['password']);
            if($user->save()){
                $model->user_id = $user->user_id;
                $logo = $this->model->upload_file('logo');
                if($logo){
                    $model->logo = $logo;
                }
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    $template = Templates::findOne(['name'=>'merchant_account']);
                    if($template){
                        $queue = new MailQueue();
                        $queue->email_to = $user->email;
                        $queue->subject = $template->subject;
                        $queue->html_body = $template->body;
                        $queue->html_body = str_replace('[NAME]', $user->first_name, $queue->html_body);
                        $queue->html_body = str_replace('[EMAIL]', $user->email, $queue->html_body);
                        $queue->html_body = str_replace('[PASSWORD]', $data['password'], $queue->html_body);
                        $queue->html_body = str_replace('[LINK]', Yii::$app->request->hostInfo.'/dashboard/account/login', $queue->html_body);
                        $queue->save();
                    }
                    $this->model->msg('Account created');
                    return $this->redirect(['view', 'id' => $model->merchant_id]);
                }
            }else{
                $this->model->error_msg($this->model->get_model_errors($user));
            }
        }
        $model->status = 1;
        return $this->render('create', [
            'model' => $model,
            'user' => new Users(),
        ]);
    }

    /**
     * Updates an existing Merchants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);        
        $data = Yii::$app->request->post();
        if($data){
            $logo = $this->model->upload_file('logo');
            if($logo){
                $model->logo = $logo;
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->user->load(Yii::$app->request->post());
            if($data['password']){
                $model->user->password = Yii::$app->getSecurity()->generatePasswordHash($data['password']);
            }
            if($model->user->save() && $model->save()){
                return $this->redirect(['view', 'id' => $model->merchant_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'user' => $model->user,
        ]);
    }

    /**
     * Deletes an existing Merchants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Merchants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Merchants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Merchants::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
