<?php 
namespace backend\controllers;
use yii;
use backend\models\Templates;
use backend\models\MailQueue;
use backend\models\Users;
use backend\models\Admins;
use backend\models\Models;

/**
**
**/

class AccountController extends \yii\web\Controller {
    public $layout = 'custom';
    public $model;

    public function init(){
        $this->model = new Models();
    }

    public function actionLogin(){
        $data = Yii::$app->request->post();
        if($data){
            $user = Users::findOne(['email'=>$data['email'],'role'=>'admin']);
            if($user){
                if(Yii::$app->getSecurity()->validatePassword($data['password'],$user->password)){
                    Yii::$app->session->set('user',$user);
                    return $this->redirect(['/']);
                }else{
                    $this->model->error_msg('Wrong Password');
                }
            }else{
                $this->model->error_msg('Wrong Email');
            }
            return $this->redirect(['login']);
        }else{
            return $this->render('//account/login');
        }
    }
    public function actionForgot(){
        $data = Yii::$app->request->post();
        if($data){
            $user = Users::findOne(['email'=>$data['email'],'role'=>'admin']);
            if($user){
                $template = Templates::findOne(['name'=>'forgot_password']);
                if($template){
                    $password = 'ocular_'.rand(2000,3000);
                    $queue = new MailQueue();
                    $queue->email_to = $data['email'];
                    $queue->subject = $template->subject;
                    $queue->html_body = $template->body;
                    $queue->html_body = str_replace('[NAME]', $user->first_name, $queue->html_body);
                    $queue->html_body = str_replace('[PASSWORD]', $password, $queue->html_body);
                    if($queue->save()){
                        $user->password = Yii::$app->getSecurity()->generatePasswordHash($password);
                        $user->save();
                        $this->model->msg('Password reset instructions sent');
                    }
                }
            }else{
                $this->model->error_msg('Wrong Email');
            }
        }
        return $this->render('//account/forgot');
    }
}
