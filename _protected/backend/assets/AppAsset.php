<?php
/**
 * -----------------------------------------------------------------------------
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * -----------------------------------------------------------------------------
 */

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

// set @themes alias so we do not have to update baseUrl every time we change themes
Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 *
 * Customized by Nenad Živković
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';
    
    public $css = [
        '/dashboard/themes/backend/assets/vendors/bootstrap/dist/css/bootstrap.min.css',
        '/dashboard/themes/backend/assets/vendors/font-awesome/css/font-awesome.min.css',
        '/dashboard/themes/backend/assets/vendors/line-awesome/css/line-awesome.min.css',
        '/dashboard/themes/backend/assets/vendors/themify-icons/css/themify-icons.css',
        '/dashboard/themes/backend/assets/vendors/animate.css/animate.min.css',
        '/dashboard/themes/backend/assets/vendors/toastr/toastr.min.css',
        '/dashboard/themes/backend/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css',
        '/dashboard/themes/backend/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css',
        '/dashboard/themes/backend/assets/css/main.min.css?v=1.0', 
        '/dashboard/themes/backend/assets/vendors/dataTables/datatables.min.css',
        '/dashboard/themes/backend/assets/css/glyphicons.css',
        '//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css',
        '//cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css',
    ];

    public $js = [
        '/dashboard/themes/backend/assets/vendors/popper.js/dist/umd/popper.min.js',
        '/dashboard/themes/backend/assets/vendors/bootstrap/dist/js/bootstrap.min.js',
        '/dashboard/themes/backend/assets/vendors/metisMenu/dist/metisMenu.min.js',
        '/dashboard/themes/backend/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js',
        '/dashboard/themes/backend/assets/vendors/jquery-idletimer/dist/idle-timer.min.js',
        '/dashboard/themes/backend/assets/vendors/toastr/toastr.min.js',
        '/dashboard/themes/backend/assets/vendors/jquery-validation/dist/jquery.validate.min.js',
        '/dashboard/themes/backend/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js',
        '/dashboard/themes/backend/assets/vendors/chart.js/dist/Chart.min.js',
        '/dashboard/themes/backend/assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
        '/dashboard/themes/backend/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js',
        '/dashboard/themes/backend/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js',
        '/dashboard/themes/backend/assets/js/app.min.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
