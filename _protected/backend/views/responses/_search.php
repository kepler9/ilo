<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ResponsesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="responses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'response_id') ?>

    <?= $form->field($model, 'driver_id') ?>

    <?= $form->field($model, 'survey_id') ?>

    <?= $form->field($model, 'comment') ?>

    <?= $form->field($model, 'photos') ?>

    <?php // echo $form->field($model, 'videos') ?>

    <?php // echo $form->field($model, 'points') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
