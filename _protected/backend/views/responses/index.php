<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use backend\models\Surveys;
use backend\models\Drivers;
$drivers = Drivers::find()->all();
$surveys = Surveys::find()->all();
$survey_id = Yii::$app->request->get('survey_id');
$driver_id = Yii::$app->request->get('driver_id');
$this->title = 'Responses';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute'=>'driver_id','label'=>'Driver','value'=>function($model){
            return $model->driver->user->first_name.' '.$model->driver->user->last_name;
        }],
        ['attribute'=>'survey_id','label'=>'Survey','value'=>function($model){
            return $model->survey->name;
        }],
        'comment:raw',
        
        'points',
        ['attribute'=>'date_created','label'=>'Date'],
        ['class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'view' => function($model){
                    return false;
                },
                'update' => function($model){
                    return false;
                }
            ],
        ],
    ];
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools"></div>
                </div>
                <div class="ibox-body">
                    <div class="responses-index">
                        <div class="pull-right">
                            <?php $form = ActiveForm::begin(['action'=>'/dashboard/responses/filter','options'=>['class'=>'form-inline']]); ?>
                                <div class="form-group">
                                    <select class="form-control" name="pagesize">
                                        <option value="20">Page Size</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="150">150</option>
                                        <option value="200">200</option>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;&nbsp;&nbsp;</div>
                                <div class="form-group">
                                    <select class="form-control" name="survey_id">
                                        <option value="">Survey</option>
                                        <?php if($surveys): foreach($surveys as $key):?>
                                            <option <?=($survey_id) ? 'selected' : ''?> value="<?=$key->survey_id?>"><?=$key->name?></option>
                                        <?php endforeach;endif;?>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;&nbsp;&nbsp;</div>
                                <div class="form-group">
                                    <select class="form-control" name="driver_id">
                                        <option value="">Driver</option>
                                        <?php if($drivers): foreach($drivers as $key):?>
                                            <option <?=($driver_id) ? 'selected' : ''?> value="<?=$key->driver_id?>"><?=$key->user->first_name.' '.$key->user->last_name?></option>
                                        <?php endforeach;endif;?>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;&nbsp;&nbsp;</div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Filter</button>
                                </div>
                            <?php ActiveForm::end();?>
                        </div>
                        <span class="clearfix"></span>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'tableOptions' => ['class' => 'data-table'],
                            'columns' => $gridColumns,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
