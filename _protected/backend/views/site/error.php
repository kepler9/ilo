<?php
use yii\helpers\Html;
$this->title = $name;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="alert alert-danger">
                        <?= nl2br(Html::encode($message)) ?>
                    </div>
                    <p>
                        The above error occurred while the Web server was processing your request.
                    </p>
                    <p>
                        Please contact us if you think this is a server error. Thank you.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>