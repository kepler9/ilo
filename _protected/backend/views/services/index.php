<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Add Services', ['create'], ['class' => '']) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="services-index">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'image','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->image.'">';
                                },'format'=>'raw'],
                                'name',
                                ['attribute'=>'service_type_id','label'=>'Service Type','value'=>function($model){
                                    return $model->serviceType->name;
                                }],
                                'description:ntext',
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->company;
                                }],
                                ['attribute'=>'price','value'=>function($model){
                                    return number_format($model->price);
                                }],
                                'location',
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status) ? 'Active' : 'Inactive';
                                }],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
