<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\ServiceTypes;
use backend\models\Merchants;
$merchants = Merchants::find()->all();
$servicetypes = ServiceTypes::find()->all();
?>
<div class="services-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <label>Service Type</label>
        <select name="Services[service_type_id]" class="form-control">
            <option value="">Select</option>
            <?php if($servicetypes): foreach($servicetypes as $key):?>
                <option <?=($key->service_type_id == $model->service_type_id) ? 'selected' : ''?> value="<?=$key->service_type_id?>"><?=$key->name?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <div class="form-group">
        <label>Merchant</label>
        <select name="Services[merchant_id]" class="form-control">
            <option value="">Select</option>
            <?php if($merchants): foreach($merchants as $key):?>
                <option <?=($key->merchant_id == $model->merchant_id) ? 'selected' : ''?> value="<?=$key->merchant_id?>"><?=$key->company?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'location')->widget(\kalyabin\maplocation\SelectMapLocationWidget::className(), [
        'attributeLatitude' => 'latitude',
        'attributeLongitude' => 'longitude',
        'googleMapApiKey' => 'AIzaSyDDFSfkjS3uUrezgeiS98YHoZkQaQtUusU',
        'draggable' => true,
    ]); ?>
    <br>
    <?= $form->field($model, 'image')->fileInput(['name' => 'image']) ?>
    <?= $form->field($model, 'status')->radioList([1=>'Active',0=>'Inactive']) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>
