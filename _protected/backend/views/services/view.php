<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Update', ['update', 'id' => $model->service_id], ['class' => '']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->service_id], [
                            'class' => '',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="services-view">

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                ['attribute'=>'image','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->image.'">';
                                },'format'=>'raw'],
                                ['attribute'=>'service_type_id','label'=>'Service Type','value'=>function($model){
                                    return $model->serviceType->name;
                                }],
                                'name',
                                'description:ntext',
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->company;
                                }],
                                'price',
                                'location',
                                'latitude',
                                'longitude',
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status) ? 'Active' : 'Inactive';
                                }],
                                'date_created',
                            ],
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
