<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
?>
<div class="templates-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php if(!$model->name):?>
	    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	<?php endif;?>
    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'body')->widget(CKEditor::className()) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>
