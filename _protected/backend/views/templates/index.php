<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="btn btn-default" href="/dashboard/templates/create" class="dropdown-item"><i class="fa fa-plus"></i> Create email template</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="templates-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                'subject',
                                'body:raw',
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
