<?php
use backend\models\Responses;
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Surveys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="btn btn-default" href="/dashboard/surveys/create" class="dropdown-item"><i class="fa fa-plus"></i> Create Survey</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="surveys-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                // ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'photo','format'=>'raw','value'=>function($model){
                                    return '<img width="80" src="/uploads/'.$model->photo.'">';
                                }],
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->company;
                                }],
                                'name',
                                'description:ntext',
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status) ? 'Active' : 'Inactive';
                                }],
                                'points_weight',
                                ['label'=>'Responses','value'=>function($model){
                                    $responses = Responses::find()->where(['survey_id'=>$model->survey_id])->count();
                                    return '<a href="/dashboard/responses/index?survey_id='.$model->survey_id.'">View ('.$responses.')</a>';
                                },'format'=>'raw'],
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
