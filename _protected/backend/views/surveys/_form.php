<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Merchants;
$merchants = Merchants::find()->where(['status'=>1])->orderby('merchant_id desc')->all();
?>

<div class="surveys-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <label>Merchant</label>
        <select name="surveys[merchant_id]" class="form-control">
            <option value="">Select</option>
            <?php if($merchants): foreach($merchants as $key):?>
                <option <?=($key->merchant_id == $model->merchant_id) ? 'selected' : ''?> value="<?=$key->merchant_id?>"><?=$key->company?></option>
            <?php endforeach;endif;?>
        </select>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'points_weight')->textInput() ?>
    
    <?= $form->field($model, 'photo')->fileInput(['name' => 'photo']) ?>

    <?= $form->field($model, 'status')->radioList([1=>'Active',0=>'Inactive']) ?>

    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>
