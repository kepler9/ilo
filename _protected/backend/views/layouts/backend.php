<?php
use backend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" type="text/css" href="/dashboard/themes/backend/assets/css/backend.css?v=<?=time()?>">
</head>
<body class="fixed-navbar">
    <?php $this->beginBody() ?>
    <div class="page-wrapper">
        <?=Yii::$app->controller->renderPartial('//common/navbar');?> 
        <?=Yii::$app->controller->renderPartial('//common/sidebar');?> 
        <div class="content-wrapper">
            <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]); 
            ?>
            <?= $content ?>
            <?=Yii::$app->controller->renderPartial('//common/footer');?> 
        </div>
    </div>
    <?=Yii::$app->controller->renderPartial('//common/search-panel');?> 
    <?=Yii::$app->controller->renderPartial('//common/pre-loader');?> 
    <?=Yii::$app->controller->renderPartial('//common/question-dialog');?> 
    <?php $this->endBody() ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.data-table').DataTable({
                "searching": false,
                "ordering": false,
                "paging": false,
                "order": [],
                "iDisplayLength": 5000,
                "lengthMenu": [[10, 25, 50, 100, 200, 300, 500,  -1], [10, 25, 50, 100, 200, 300, 500, "All"]],
                 dom: 'Bfrtip',
                 buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                 ]
            });
            $('.data-table').addClass('table table-bordered');
        });
</script>
<script src="/themes/frontend/js/jquery.form-validator.min.js"></script>
<script type="text/javascript">
      $.validate({
        lang: 'en'
        });
  </script>
</body>
</html>
<?php $this->endPage() ?>
