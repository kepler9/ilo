<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Ads */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Update', ['update', 'id' => $model->ad_id], ['class' => '']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->ad_id], [
                            'class' => '',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="ads-view">

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                ['attribute'=>'banner','format'=>'raw','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->banner.'">';
                                }],
                                'name',
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->user->first_name.' '.$model->merchant->user->last_name;
                                }],
                                'clicks',
                                'link:url',
                                ['attribute'=>'service_type_id','label'=>'Service Type','value'=>function($model){
                                    return $model->service_type_id ? $model->serviceType->name : '';
                                }],
                                ['attribute'=>'category_id','label'=>'Category','value'=>function($model){
                                    return $model->category_id ? $model->category->name : '';
                                }],
                                ['attribute'=>'status','value'=>function($model){
                                    return $model->status ? 'Active' : 'Inactive';
                                }],
                                'date_created',
                            ],
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
