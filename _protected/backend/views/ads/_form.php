<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Merchants;
use backend\models\ServiceTypes;
use backend\models\Categories;
$categories = Categories::find()->all();
$servicetypes = ServiceTypes::find()->all();
$merchants = Merchants::find()->all();
?>

<div class="ads-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <label>Merchant</label>
        <select data-validation="required" name="Ads[merchant_id]" class="form-control">
            <option value="">Select</option>
            <?php if($merchants): foreach($merchants as $key):?>
                <option <?=($key->merchant_id == $model->merchant_id) ? 'selected' : ''?> value="<?=$key->merchant_id?>"><?=$key->company?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <div class="form-group">
        <label>Associate with Service Type</label>
        <select name="Ads[service_type_id]" class="form-control">
            <option value="">Select</option>
            <?php if($servicetypes): foreach($servicetypes as $key):?>
                <option <?=($key->service_type_id == $model->service_type_id) ? 'selected' : ''?> value="<?=$key->service_type_id?>"><?=$key->name?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <div class="form-group">
        <label>Associate with Category</label>
        <select name="Ads[category_id]" class="form-control">
            <option value="">Select</option>
            <?php if($categories): foreach($categories as $key):?>
                <option <?=($key->category_id == $model->category_id) ? 'selected' : ''?> value="<?=$key->category_id?>"><?=$key->name?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <?= $form->field($model, 'link')->textInput(['maxlength' => true,'id'=>'_ads_link']) ?>
    <?= $form->field($model, 'banner')->fileInput(['name' => 'banner','id'=>'_ads_banner']) ?>
    <?= $form->field($model, 'status')->radioList([1=>'Active',0=>'Inactive']) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>
