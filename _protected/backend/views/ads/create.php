<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Ads */

$this->title = 'Create Ads';
$this->params['breadcrumbs'][] = ['label' => 'Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-body">
                	<div class="ads-create">

					    <?= $this->render('_form', [
					        'model' => $model,
					    ]) ?>

					</div>
                </div>
            </div>
        </div>
    </div>
</div>
