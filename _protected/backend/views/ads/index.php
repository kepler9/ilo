<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Ads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Create Ads', ['create'], ['class' => '']) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="ads-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'banner','format'=>'raw','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->banner.'">';
                                }],
                                'name',
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->company;
                                }],
                                'clicks',
                                'link:url',
                                ['attribute'=>'service_type_id','label'=>'Service Type','value'=>function($model){
                                    return $model->service_type_id ? $model->serviceType->name : '';
                                }],
                                ['attribute'=>'category_id','label'=>'Category','value'=>function($model){
                                    return $model->category_id ? $model->category->name : '';
                                }],
                                ['attribute'=>'status','value'=>function($model){
                                    return $model->status ? 'Active' : 'Inactive';
                                }],
                                'date_created',
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
