<?php
    $this->title = 'Bulk SMS';
    use yii\widgets\ActiveForm;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-list"></i></a>
                        <div class="dropdown-menu dropdown-menu-right"></div>
                    </div>
                </div>
                <div class="ibox-body">
                    <?php $form = ActiveForm::begin(['action'=>'/dashboard/messages/send']);?>
                        <div class="form-group">
                            <label>Recipient</label>
                            <select id="recipient" class="form-control" data-validation="required" name="recipient">
                                <option value="">Select</option>
                                <option value="drivers">Drivers</option>
                                <option value="merchants">Merchants</option>
                                <option value="phone">Enter Phone Number</option>
                            </select>
                        </div>
                        <div style="display: none" class="form-group phone-number-form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone"  data-validation="number" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea id="textarea" class="form-control" name="message" data-validation="required" rows="4"></textarea>
                            <p style="color:#777"><span id="chars">0</span> characters</p>
                        </div>
                        <div class="form-group pull-right">
                            <button class="btn btn-success">Send</button>
                        </div>
                        <span class="clearfix"></span>
                    <?php ActiveForm::end();?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#recipient').change(function(event) {
        if($(this).val() == 'phone'){
            $('.phone-number-form-group').css('display', 'block');
        }else{
            $('.phone-number-form-group').css('display', 'none');
        }
    });
    $('#textarea').keyup(function() {
        var length = $(this).val().length;
        $('#chars').text(length);
    });
</script>