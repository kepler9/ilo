<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->user->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Update', ['update', 'id' => $model->driver_id], ['class' => '']) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="drivers-view">
                        
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                 ['attribute'=>'user_id','label'=>'First Name','value'=>function($model){
                                    return $model->user->first_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Last Name','value'=>function($model){
                                    return $model->user->last_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Email','value'=>function($model){
                                    return $model->user->email;
                                },'format'=>'email'],
                                ['attribute'=>'user_id','label'=>'Phone','value'=>function($model){
                                    return $model->user->phone;
                                }],
                                'car_type',
                                'car_model',
                                'plate_number',
                                'car_color',
                                'monthly_fuel_budget',
                                'servicing_mileage',
                                'insurance_renewal_date',
                                'tyre_rotation_date',
                                'wheel_balancing_alignment_date',
                                'psv_ntsa_inspection',
                                'driving_license_renewal_date',
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status == 1) ? 'Active' : 'Inactive';
                                }],
                                ['attribute'=>'loyalty_points','value'=>function($model){
                                    return number_format($model->loyalty_points);
                                }],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
