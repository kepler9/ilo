<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Drivers';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="btn btn-default" href="/dashboard/drivers/create" class="dropdown-item"><i class="fa fa-plus"></i> Create a driver account</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="drivers-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                // ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'user_id','label'=>'First Name','value'=>function($model){
                                    return $model->user->first_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Last Name','value'=>function($model){
                                    return $model->user->last_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Email','value'=>function($model){
                                    return $model->user->email;
                                },'format'=>'email'],
                                ['attribute'=>'user_id','label'=>'Phone','value'=>function($model){
                                    return $model->user->phone;
                                }],
                                'car_type',
                                'car_model',
                                'plate_number',
                                'car_color',
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status == 1) ? 'Active' : 'Inactive';
                                }],
                                ['attribute'=>'loyalty_points','value'=>function($model){
                                    return number_format($model->loyalty_points);
                                }],
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
