<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DriversSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drivers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'driver_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'loyalty_points') ?>

    <?= $form->field($model, 'car_type') ?>

    <?= $form->field($model, 'car_model') ?>

    <?php // echo $form->field($model, 'plate_number') ?>

    <?php // echo $form->field($model, 'car_color') ?>

    <?php // echo $form->field($model, 'monthly_fuel_budget') ?>

    <?php // echo $form->field($model, 'servicing_mileage') ?>

    <?php // echo $form->field($model, 'insurance_renewal_date') ?>

    <?php // echo $form->field($model, 'tyre_rotation_date') ?>

    <?php // echo $form->field($model, 'wheel_balancing_alignment_date') ?>

    <?php // echo $form->field($model, 'psv_ntsa_inspection') ?>

    <?php // echo $form->field($model, 'driving_license_renewal_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
