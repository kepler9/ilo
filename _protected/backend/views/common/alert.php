<?php if(Yii::$app->session->getFlash('msg')):?>
	<div class="alert alert-<?=(Yii::$app->session->getFlash('error_msg')) ? 'danger' : 'success'; ?> alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
		<p><?=Yii::$app->session->getFlash('msg')?></p>
	</div>
<?php endif;?>
