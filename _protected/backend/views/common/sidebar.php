<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <ul class="side-menu metismenu">
            <li>
                <a href="/dashboard/drivers/index"><i class="sidebar-item-icon fa fa-car"></i>
                    <span class="nav-label">Drivers</span>
                </a>
            </li>
            <li>
                <a href="/dashboard/merchants/index"><i class="sidebar-item-icon fa fa-building-o"></i>
                    <span class="nav-label">Merchants</span>
                </a>
            </li>
            <li>
                <a href="/dashboard/accessories/index"><i class="sidebar-item-icon fa fa-wrench"></i>
                    <span class="nav-label">Accessories</span>
                </a>
            </li>
            <li>
                <a href="/dashboard/services/index"><i class="sidebar-item-icon fa fa-cog"></i>
                    <span class="nav-label">Services</span>
                </a>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bell"></i>
                    <span class="nav-label">Orders</span><i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="/dashboard/servicesorders/index">Services</a>
                    </li>
                    <li>
                        <a href="/dashboard/accessoriesorders/index">Accessories</a>
                    </li>
                </ul>
            </li>
             <li>
                <a href="/dashboard/surveys/index"><i class="sidebar-item-icon fa fa-pencil"></i>
                    <span class="nav-label">Surveys</span>
                </a>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bar-chart"></i>
                    <span class="nav-label">Reports</span><i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="/dashboard/fuelusage/index">Fuel Usage</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="/dashboard/messages/bulk"><i class="sidebar-item-icon fa fa-mobile"></i>
                    <span class="nav-label">Bulk SMS</span>
                </a>
            </li>
            
            <?php if($this->context->admin->level == 1):?>
                <li>
                    <a href="javascript:;"><i class="sidebar-item-icon ti-settings"></i>
                        <span class="nav-label">Settings</span><i class="fa fa-angle-left arrow"></i>
                    </a>
                    <ul class="nav-2-level collapse">
                        <li>
                            <a href="/dashboard/ads/index">Ads Manager</a>
                        </li>
                        <li>
                            <a href="/dashboard/servicetypes/index">Service Types</a>
                        </li>
                        <li>
                            <a href="/dashboard/categories/index">Categories</a>
                        </li>
                        <li>
                            <a href="/dashboard/vouchers/index">Vouchers</a>
                        </li>
                        <li>
                            <a href="/dashboard/admins/index">Admins</a>
                        </li>
                        <li>
                            <a href="/dashboard/templates/index">Email Templates</a>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-user-o"></i>
                    <span class="nav-label">My Account</span><i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="/dashboard/admins/view">Profile</a>
                    </li>
                    <li>
                        <a href="/welcome/logout">Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="sidebar-footer"></div>
    </div>
</nav>
