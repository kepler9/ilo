<footer class="page-footer">
    <div class="font-13"><?=date('Y')?> © <b>ILO ENTERPRISE LTD</b></div>
    <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>
