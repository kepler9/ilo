<header class="header">
    <div class="page-brand">
        <a href="/dashboard">
            <span class="brand">ILO ADMIN</span>
            <span class="brand-mini">ILO</span>
        </a>
    </div>
    <div class="flexbox flex-1">
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </li>
        </ul>
        <ul class="nav navbar-toolbar">
            <li><a href="https://ilo.co.ke/">App Landing Page</a></li>
            <li><a href="/welcome/logout">Logout (<?=$this->context->user->first_name.' '.$this->context->user->last_name?>)</a></li>
            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" href="/dashboard/admins/view">
                    <img src="/uploads/avatar.png" alt="image" />
                </a>
            </li>
        </ul>
    </div>
</header>