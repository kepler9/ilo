<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FuelUsageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fuel-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fuel_usage_id') ?>

    <?= $form->field($model, 'driver_id') ?>

    <?= $form->field($model, 'current_mileage') ?>

    <?= $form->field($model, 'fuel_amount') ?>

    <?= $form->field($model, 'fuel_cost') ?>

    <?php // echo $form->field($model, 'merchant_id') ?>

    <?php // echo $form->field($model, 'date_posted') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
