<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Fuel Usages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="fuel-usage-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'tableOptions' => ['class' => 'data-table'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'driver_id','label'=>'Driver','value'=>function($model){
                                    return $model->driver->user->first_name.' '.$model->driver->user->last_name;
                                }],
                                ['attribute'=>'current_mileage','value'=>function($model){
                                    return number_format($model->current_mileage);
                                }],
                                ['attribute'=>'fuel_amount','value'=>function($model){
                                    return number_format($model->fuel_amount);
                                }],
                                ['attribute'=>'fuel_cost','value'=>function($model){
                                    return number_format($model->fuel_cost);
                                }],
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->company;
                                },'label'=>'Service Provider'],
                                'date_posted',
                                // ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
