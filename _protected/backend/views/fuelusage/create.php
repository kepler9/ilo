<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FuelUsage */

$this->title = 'Create Fuel Usage';
$this->params['breadcrumbs'][] = ['label' => 'Fuel Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fuel-usage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
