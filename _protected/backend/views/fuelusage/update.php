<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FuelUsage */

$this->title = 'Update Fuel Usage: ' . $model->fuel_usage_id;
$this->params['breadcrumbs'][] = ['label' => 'Fuel Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fuel_usage_id, 'url' => ['view', 'id' => $model->fuel_usage_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fuel-usage-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
