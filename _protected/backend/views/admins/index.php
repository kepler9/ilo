<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="btn btn-default" href="/dashboard/admins/create" class="dropdown-item"><i class="fa fa-plus"></i> Create admin account</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="admins-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'user_id','label'=>'First Name','value'=>function($model){
                                    return $model->user->first_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Last Name','value'=>function($model){
                                    return $model->user->last_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Email','value'=>function($model){
                                    return $model->user->email;
                                },'format'=>'email'],
                                ['attribute'=>'user_id','label'=>'Phone','value'=>function($model){
                                    return $model->user->phone;
                                }],
                                ['attribute'=>'level','value'=>function($model){
                                    return ($model->level == 1) ? 'Super Admin' : 'System Admin';
                                }],
                                ['class' => 'yii\grid\ActionColumn',
                                    'buttons' => [
                                        'view' => function($model){
                                            return false;
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
