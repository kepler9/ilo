<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="admins-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($user, 'first_name')->textInput() ?>
    <?= $form->field($user, 'last_name')->textInput() ?>
    <?= $form->field($user, 'phone')->textInput() ?>
    <?= $form->field($user, 'email')->textInput() ?>
    <div class="form-group">
    	<label>Password</label>
    	<input type="text" class="form-control" placeholder="Enter password" name="password">
    </div>
    <?= $form->field($model, 'level')->radioList([
    	2=>'System Admin',
    	1=>'Super Admin (Can manage other admins, modify system settings)',
    ]) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix">
    <?php ActiveForm::end(); ?>
</div>
