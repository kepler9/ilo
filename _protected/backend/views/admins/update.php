<?php
use yii\helpers\Html;
$this->title = 'Update Admin: ' . $model->user->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Admins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->first_name.' '.$model->user->last_name, 'url' => ['view', 'id' => $model->admin_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-body">
                	<div class="admins-update">

					    <?= $this->render('_form', [
					        'model' => $model,
					        'user' => $user,
					    ]) ?>

					</div>
                </div>
            </div>
        </div>
    </div>
</div>
