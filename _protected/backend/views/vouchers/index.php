<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Vouchers';
$this->params['breadcrumbs'][] = $this->title;
use backend\models\Redeemed;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="btn btn-default" href="/dashboard/vouchers/create" class="dropdown-item"><i class="fa fa-plus"></i> Create a voucher</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="vouchers-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'name',
                                'description:ntext',
                                'points',
                                ['label'=>'Total Redeemed','value'=>function($model){
                                    return '<a href="/dashboard/redeemed/index?voucher_id='.$model->voucher_id.'">View ('.Redeemed::find()->where(['voucher_id'=>$model->voucher_id])->count().')</a>';
                                },'format'=>'raw'],
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status) ? 'Active' : 'Inactive';
                                }],
                                'date_created',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
