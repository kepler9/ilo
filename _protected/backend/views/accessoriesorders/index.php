<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Accessories Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="orders-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'tableOptions' => ['class' => 'data-table'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'accessory_id','label'=>'Accessory','value'=>function($model){
                                    return $model->accessory->name;
                                }],
                                ['attribute'=>'driver_id','label'=>'Driver','value'=>function($model){
                                    return $model->driver->user->first_name.' '.$model->driver->user->last_name;
                                }],
                                'quantity',
                                'amount',
                                ['attribute'=>'accessory_id','label'=>'Commission','value'=>function($model){
                                    return number_format($model->accessory->category->commission/100 * $model->amount);
                                }],
                                'status',
                                'date_ordered',
                                ['class' => 'yii\grid\ActionColumn',
                                    'buttons' => ['view'=>function($model){
                                        return false;
                                    }],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
