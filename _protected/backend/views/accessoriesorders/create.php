<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AccessoriesOrders */

$this->title = 'Create Accessories Orders';
$this->params['breadcrumbs'][] = ['label' => 'Accessories Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accessories-orders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
