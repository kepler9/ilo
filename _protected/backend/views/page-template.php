<?php
$this->title = 'Welcome Admin';
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-list"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item"><i class="ti-pencil mr-2"></i>Action 1</a>
                            <a href="#" class="dropdown-item"><i class="ti-pencil-alt mr-2"></i>Action 2</a>
                            <a href="#" class="dropdown-item"><i class="ti-close mr-2"></i>Action 3</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">

                </div>
            </div>
        </div>
    </div>
</div>