<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Accessories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Update', ['update', 'id' => $model->accessory_id], ['class' => '']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->accessory_id], [
                            'class' => '',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="accessories-view">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                ['attribute'=>'photo','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->photo.'">';
                                },'format'=>'raw'],
                                ['attribute'=>'category_id','label'=>'Category','value'=>function($model){
                                    return $model->category->name;
                                }],
                                'name',
                                'description:ntext',
                                'price',
                                ['attribute'=>'merchant_id','label'=>'Merchant','value'=>function($model){
                                    return $model->merchant->company;
                                }],
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status) ? 'Active' : 'Inactive';
                                }],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
