<?php
use yii\helpers\Html;
$this->title = 'Update Accessories: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Accessories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->accessory_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-body">
                	<div class="accessories-update">

					    <?= $this->render('_form', [
					        'model' => $model,
					    ]) ?>

					</div>
                </div>
            </div>
        </div>
    </div>
</div>
