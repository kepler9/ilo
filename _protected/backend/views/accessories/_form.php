<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Categories;
use backend\models\Merchants;
$merchants = Merchants::find()->all();
$categories = Categories::find()->all();
?>
<div class="accessories-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <label>Merchant</label>
        <select data-validation="required" name="Accessories[merchant_id]" class="form-control">
            <option value="">Select</option>
            <?php if($merchants): foreach($merchants as $key):?>
                <option <?=($key->merchant_id == $model->merchant_id) ? 'selected' : ''?> value="<?=$key->merchant_id?>"><?=$key->company?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <div class="form-group">
        <label>Category</label>
        <select data-validation="required" name="Accessories[category_id]" class="form-control">
            <option value="">Select</option>
            <?php if($categories): foreach($categories as $key):?>
                <option <?=($key->category_id == $model->category_id) ? 'selected' : ''?> value="<?=$key->category_id?>"><?=$key->name?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'price')->textInput() ?>
    <br>
    <?= $form->field($model,'photo')->fileInput(['name'=>'photo']);?>
    <br>
    <?= $form->field($model, 'status')->radioList([1=>'Active',0=>'Inactive']) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>
