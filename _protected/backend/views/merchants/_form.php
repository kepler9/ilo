<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="merchants-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
    <?= $form->field($user, 'first_name')->textInput() ?>
    <?= $form->field($user, 'last_name')->textInput() ?>
    <?= $form->field($user, 'phone')->textInput() ?>
    <?= $form->field($user, 'email')->textInput() ?>
    <div class="form-group">
    	<label>Password</label>
    	<input class="form-control" placeholder="Enter password" name="password">
    </div>
    <?= $form->field($model, 'location')->widget(\kalyabin\maplocation\SelectMapLocationWidget::className(), [
        'attributeLatitude' => 'latitude',
        'attributeLongitude' => 'longitude',
        'googleMapApiKey' => 'AIzaSyDDFSfkjS3uUrezgeiS98YHoZkQaQtUusU',
        'draggable' => true,
    ]); ?>
    <br>
    <?= $form->field($model, 'logo')->fileInput(['name' => 'logo']) ?>
    <br>
    <?= $form->field($model, 'status')->radioList([1=>'Active',0=>'Inactive']) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>
