<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Merchants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <a class="btn btn-default" href="/dashboard/merchants/create" class="dropdown-item"><i class="fa fa-plus"></i> Create a merchant account</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="merchants-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                // ['class' => 'yii\grid\SerialColumn'],
                                ['attribute'=>'logo','format'=>'raw','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->logo.'">';
                                }],
                                'company',
                                ['attribute'=>'user_id','label'=>'First Name','value'=>function($model){
                                    return $model->user->first_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Last Name','value'=>function($model){
                                    return $model->user->last_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Email','value'=>function($model){
                                    return $model->user->email;
                                },'format'=>'email'],
                                ['attribute'=>'user_id','label'=>'Phone','value'=>function($model){
                                    return $model->user->phone;
                                }],
                                'location',
                                'rating',
                                ['attribute'=>'merchant_id','label'=>'Accessories','format'=>'raw','value'=>function($model){
                                    return '<a href="/dashboard/accessories/index?merchant_id='.$model->merchant_id.'">'.count($model->accessories).' > view</a>';
                                }],
                                ['attribute'=>'merchant_id','label'=>'Services','format'=>'raw','value'=>function($model){
                                    return '<a href="/dashboard/services/index?merchant_id='.$model->merchant_id.'">'.count($model->services).' > view</a>';
                                }],
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status == 1) ? 'Active' : 'Inactive';
                                }],
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
