<?php
use yii\helpers\Html;
$this->title = 'Update Merchants: ' . $model->company;
$this->params['breadcrumbs'][] = ['label' => 'Merchants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->company, 'url' => ['view', 'id' => $model->merchant_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools"></div>
                </div>
                <div class="ibox-body">
                	<div class="merchants-update">
					    <?= $this->render('_form', [
					        'model' => $model,
					        'user' => $user,
					    ]) ?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
