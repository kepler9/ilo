<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->company;
$this->params['breadcrumbs'][] = ['label' => 'Merchants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Update', ['update', 'id' => $model->merchant_id], ['class' => '']) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="merchants-view">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                ['attribute'=>'logo','format'=>'raw','value'=>function($model){
                                    return '<img width="70" src="/uploads/'.$model->logo.'">';
                                }],
                                ['attribute'=>'user_id','label'=>'First Name','value'=>function($model){
                                    return $model->user->first_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Last Name','value'=>function($model){
                                    return $model->user->last_name;
                                }],
                                ['attribute'=>'user_id','label'=>'Email','value'=>function($model){
                                    return $model->user->email;
                                },'format'=>'email'],
                                ['attribute'=>'user_id','label'=>'Phone','value'=>function($model){
                                    return $model->user->phone;
                                }],
                                'company',
                                'rating',
                                'location',
                                'latitude',
                                'longitude',
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status == 1) ? 'Active' : 'Inactive';
                                }],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
