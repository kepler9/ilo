<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'status')->radioList(['pending'=>'Pending','processed' => 'Processed','cancelled'=>'Cancelled']) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>
