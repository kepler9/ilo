<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Services Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="orders-index">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'tableOptions' => ['class' => 'data-table'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                ['attribute'=>'service_id','label'=>'Service','value'=>function($model){
                                    return $model->service->name;
                                }],
                                ['attribute'=>'driver_id','label'=>'Driver','value'=>function($model){
                                    return $model->driver->user->first_name.' '.$model->driver->user->last_name;
                                }],
                                ['attribute'=>'amount','value'=>function($model){
                                    return number_format($model->amount);
                                }],
                                ['attribute'=>'service_type_id','label'=>'Commission','value'=>function($model){
                                    return number_format($model->service->serviceType->commission/100 * $model->amount);
                                }],
                                'status',
                                'date_ordered',

                                ['class' => 'yii\grid\ActionColumn',
                                    'buttons' => ['view'=>function($model){
                                        return false;
                                    }],
                                ],
                            ],
                        ]); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
