<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ServicesOrders */

$this->title = 'Create Services Orders';
$this->params['breadcrumbs'][] = ['label' => 'Services Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-orders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
