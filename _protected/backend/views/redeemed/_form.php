<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="redeemed-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'status')->radioList([1=>'Approved',0=>'Pending']) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>
