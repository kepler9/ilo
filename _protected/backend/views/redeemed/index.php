<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Drivers Redeemed Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools"></div>
                </div>
                <div class="ibox-body">
                    <div class="redeemed-index">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                ['attribute'=>'voucher_id','label'=>'Voucher','value'=>function($model){
                                    return $model->voucher->name;
                                }],
                                ['attribute'=>'driver_id','label'=>'Driver','value'=>function($model){
                                    return $model->driver->user->first_name.' '.$model->driver->user->last_name;
                                }],
                                ['attribute'=>'status','value'=>function($model){
                                    return ($model->status) ? 'Approved' : 'Pending';
                                }],
                                ['attribute'=>'date_created','label'=>'Date Redeemed'],

                                ['class' => 'yii\grid\ActionColumn',
                                    'buttons' => [
                                        'view' => function(){
                                            return false;
                                        }
                                    ],
                                ],
                            ],
                        ]); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
