<?php
use yii\helpers\Html;
$this->title = 'Update ' . $model->contributor->user->first_name."'s". ' Request';
$this->params['breadcrumbs'][] = ['label' => 'drivers Redeemed Points', 'url' => ['index?voucher_id='.$model->voucher_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools"></div>
                </div>
                <div class="ibox-body">
                	<div class="redeemed-update">

						    <?= $this->render('_form', [
						        'model' => $model,
						    ]) ?>

						</div>
                </div>
            </div>
        </div>
    </div>
</div>
