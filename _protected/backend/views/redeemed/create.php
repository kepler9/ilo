<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Redeemed */

$this->title = 'Create Redeemed';
$this->params['breadcrumbs'][] = ['label' => 'Redeemeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redeemed-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
