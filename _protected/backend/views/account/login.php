<?php $this->title = 'Admin login'; ?>
 <style>
    body {
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url(/dashboard/themes/backend/assets/img/ilo-bg-4.jpeg);
    }
    .cover {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(117, 54, 230, .1);
    }
    .auth-head-icon {
        position: relative;
        height: 60px;
        width: 60px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        font-size: 30px;
        background: rgba(0, 0, 0, .6);
        color: #fff;
        box-shadow: 0 5px 20px #d6dee4;
        border-radius: 50%;
        z-index: 2;
    }
    .login-box {
        background: rgba(0, 0, 0, .6);
        color: rgba(255, 255, 255, .8);
    }
    .login-box .form-control {
        background-color: transparent;
        border-color: rgba(255, 255, 255, .6);
        color: #fff;
    }
    .login-box .form-control:focus {
        border-color: rgba(255, 255, 255, 1);
    }
</style>
<?php use yii\widgets\ActiveForm;?>
<div class="cover"></div>
<div style="max-width: 400px;margin: 100px auto 50px;height: 500px!important;">
    <div class="text-center mb-5">
        <span class="auth-head-icon"><i class="la la-user"></i></span>
    </div>
    <div class="ibox login-box">
        <?php if(Yii::$app->session->getFlash('msg')):?>
            <div class="alert alert-<?=(Yii::$app->session->getFlash('error_msg')) ? 'danger' : 'success';?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
                <p><?=Yii::$app->session->getFlash('msg')?></p>
            </div>
        <?php endif;?>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'ibox-body','id' => 'login-form']]);?>
            <h4 class="font-strong text-center mb-5">ADMIN LOG IN</h4>
            <div class="form-group mb-4">
                <input autocomplete="off" class="form-control form-control-line" type="text" name="email" placeholder="Email">
            </div>
            <div class="form-group mb-4">
                <input autocomplete="off" class="form-control form-control-line" type="password" name="password" placeholder="Password">
            </div>
            <div class="flexbox mb-5">
                <label class="checkbox checkbox-primary">
                    <input type="checkbox" checked="">
                    <span class="input-span"></span> Remember</label>
                <a class="color-inherit" href="/dashboard/account/forgot">Forgot Password?</a>
            </div>
            <!-- <div class="form-group">
                <a class="color-inherit" href="/">Back to website</a>
            </div> -->
            <div class="text-center mb-4">
                <button class="btn btn-primary btn-rounded btn-block">LOGIN</button>
            </div>
        <?php ActiveForm::end();?>
    </div>
</div>
 <script>
    $(function() {
        $('#login-form').validate({
            errorClass: "help-block",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
    });
</script>
