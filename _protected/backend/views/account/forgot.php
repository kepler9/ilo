<?php $this->title = 'Forgot Password'; ?>
<style>
    body {
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url(/dashboard/themes/backend/assets/img/ilo-bg-4.jpeg);
    }
    .cover {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(117, 54, 230, .1);
    }
    .login-content {
        max-width: 400px;
        margin: 100px auto 50px;
        top: 90px;
    }
    .auth-head-icon {
        position: relative;
        height: 60px;
        width: 60px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        font-size: 30px;
        background-color: #fff;
        color: #5c6bc0;
        box-shadow: 0 5px 20px #d6dee4;
        border-radius: 50%;
        transform: translateY(-50%);
        z-index: 2;
    }
</style>
<?php use yii\widgets\ActiveForm;?>
<div class="cover"></div>
<div class="ibox login-content">
    <div class="text-center">
        <span class="auth-head-icon"><i class="la la-key"></i></span>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'ibox-body pt-0','id'=>'forgot-form']]);?>
        <h4 class="font-strong text-center mb-4">FORGOT PASSWORD</h4>
        <?php if(Yii::$app->session->getFlash('msg')):?>
            <div class="alert alert-<?=(Yii::$app->session->getFlash('error_msg')) ? 'danger' : 'success';?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
                <p><?=Yii::$app->session->getFlash('msg')?></p>
            </div>
        <?php endif;?>
        <p class="mb-4">Enter your email address below and we'll send you password reset instructions.</p>
        <div class="form-group mb-4">
            <input class="form-control form-control-line" type="text" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            <a class="color-inherit" href="/dashboard/account/login">Back to Login</a>
        </div>
        <div class="text-center">
            <button class="btn btn-primary btn-rounded btn-block btn-air">SUBMIT</button>
        </div>
    <?php ActiveForm::end();?>
</div>
<script>
    $(function() {
        $('#forgot-form').validate({
            errorClass: "help-block",
            rules: {
                email: {
                    required: true,
                    email: true
                },
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
    });
</script>
