<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Service Types';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-content fade-in-up">
    <?=Yii::$app->controller->renderPartial('//common/alert');?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?=$this->title?></div>
                    <div class="ibox-tools">
                        <?= Html::a('Create Service Types', ['create'], ['class' => '']) ?>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="service-types-index">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'name',
                                'commission',
                                'description:ntext',
                                'date_created',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
