<?php
    $this->title = 'Welcome Admin';
    $stats = $this->context->model->summary_statistics();
?>
<div class="page-content fade-in-up">
    <div class="row mb-4">
        <div class="col-lg-3 col-md-6">
            <div class="card mb-4">
                <div onclick="window.location='/dashboard/drivers/index'" class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="73" data-bar-color="#18C5A9" data-size="80" data-line-width="8">
                        <span class="easypie-data text-success" style="font-size:32px;">
                    </div>
                    <div>
                        <h3 class="font-strong text-success"><?=$stats->drivers?></h3>
                        <div class="text-muted">Drivers</div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card mb-4">
                <div onclick="window.location='/dashboard/merchants/index'" class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="42" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                        <span class="easypie-data font-26 text-primary">
                    </div>
                    <div>
                        <h3 class="font-strong text-primary"><?=$stats->merchants?></h3>
                        <div class="text-muted">Merchants</div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card mb-4">
                <div onclick="window.location='/dashboard/services/index'" class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="70" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                        <span class="easypie-data text-pink" style="font-size:32px;">
                    </div>
                    <div>
                        <h3 class="font-strong text-pink"><?=$stats->services?></h3>
                        <div class="text-muted">Services</div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card mb-4">
                <div onclick="window.location='/dashboard/servicesorders/index'" class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="70" data-bar-color="#F39C12" data-size="80" data-line-width="8">
                        <span class="easypie-data text-warning" style="font-size:32px;">
                    </div>
                    <div>
                        <h3 class="font-strong  text-warning"><?=$stats->orders?></h3>
                        <div class="text-muted">Orders</div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
