<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\FuelUsage;

/**
 * FuelUsageSearch represents the model behind the search form of `backend\models\FuelUsage`.
 */
class FuelUsageSearch extends FuelUsage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fuel_usage_id', 'driver_id', 'merchant_id'], 'integer'],
            [['current_mileage', 'fuel_amount', 'fuel_cost'], 'number'],
            [['date_posted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FuelUsage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fuel_usage_id' => $this->fuel_usage_id,
            'driver_id' => $this->driver_id,
            'current_mileage' => $this->current_mileage,
            'fuel_amount' => $this->fuel_amount,
            'fuel_cost' => $this->fuel_cost,
            'merchant_id' => $this->merchant_id,
            'date_posted' => $this->date_posted,
        ]);

        return $dataProvider;
    }
}
