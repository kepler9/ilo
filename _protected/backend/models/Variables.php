<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "variables".
 *
 * @property int $id
 * @property double $commission
 * @property double $balance
 * @property double $sms_balance
 * @property double $payout_charges
 */
class Variables extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'variables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['commission', 'balance', 'sms_balance', 'payout_charges'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'commission' => 'Commission',
            'balance' => 'Balance',
            'sms_balance' => 'Sms Balance',
            'payout_charges' => 'Payout Charges',
        ];
    }
}
