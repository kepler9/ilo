<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AccessoriesOrders;

/**
 * AccessoriesOrdersSearch represents the model behind the search form of `backend\models\AccessoriesOrders`.
 */
class AccessoriesOrdersSearch extends AccessoriesOrders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'accessory_id', 'driver_id', 'quantity'], 'integer'],
            [['amount'], 'number'],
            [['status', 'date_ordered'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccessoriesOrders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'accessory_id' => $this->accessory_id,
            'driver_id' => $this->driver_id,
            'amount' => $this->amount,
            'quantity' => $this->quantity,
            'date_ordered' => $this->date_ordered,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
