<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "accessories".
 *
 * @property int $accessory_id
 * @property int $merchant_id
 * @property int $category_id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property string $photo
 * @property int $status
 * @property string $date_created
 *
 * @property Categories $category
 * @property Merchants $merchant
 * @property AccessoriesOrders[] $accessoriesOrders
 */
class Accessories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accessories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['merchant_id', 'category_id', 'name', 'description', 'price'], 'required'],
            [['merchant_id', 'category_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['date_created'], 'safe'],
            [['name', 'photo'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'category_id']],
            [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Merchants::className(), 'targetAttribute' => ['merchant_id' => 'merchant_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'accessory_id' => 'Accessory ID',
            'merchant_id' => 'Merchant ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'photo' => 'Photo',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchants::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessoriesOrders()
    {
        return $this->hasMany(AccessoriesOrders::className(), ['accessory_id' => 'accessory_id']);
    }
}
