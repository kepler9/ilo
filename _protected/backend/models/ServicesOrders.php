<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "services_orders".
 *
 * @property int $id
 * @property int $service_id
 * @property int $driver_id
 * @property double $amount
 * @property string $status
 * @property string $location
 * @property string $odometer
 * @property string $date_ordered
 *
 * @property Drivers $driver
 * @property Services $service
 */
class ServicesOrders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'driver_id', 'amount'], 'required'],
            [['service_id', 'driver_id'], 'integer'],
            [['amount'], 'number'],
            [['date_ordered'], 'safe'],
            [['status', 'location', 'odometer'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drivers::className(), 'targetAttribute' => ['driver_id' => 'driver_id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'service_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'driver_id' => 'Driver ID',
            'amount' => 'Amount',
            'status' => 'Status',
            'location' => 'Location',
            'odometer' => 'Odometer',
            'date_ordered' => 'Date Ordered',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Drivers::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['service_id' => 'service_id']);
    }
}
