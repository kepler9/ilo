<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sms_queue".
 *
 * @property int $id
 * @property string $phone_number
 * @property string $message
 * @property string $date_created
 */
class SmsQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone_number', 'message'], 'required'],
            [['message'], 'string'],
            [['date_created'], 'safe'],
            [['phone_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone_number' => 'Phone Number',
            'message' => 'Message',
            'date_created' => 'Date Created',
        ];
    }
}
