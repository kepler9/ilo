<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "responses".
 *
 * @property int $response_id
 * @property int $driver_id
 * @property int $survey_id
 * @property string $comment
 * @property string $photos
 * @property string $videos
 * @property int $points
 * @property string $date_created
 *
 * @property Drivers $driver
 * @property Surveys $survey
 */
class Responses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'responses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['driver_id', 'survey_id', 'comment'], 'required'],
            [['driver_id', 'survey_id', 'points'], 'integer'],
            [['comment', 'photos', 'videos'], 'string'],
            [['date_created'], 'safe'],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drivers::className(), 'targetAttribute' => ['driver_id' => 'driver_id']],
            [['survey_id'], 'exist', 'skipOnError' => true, 'targetClass' => Surveys::className(), 'targetAttribute' => ['survey_id' => 'survey_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'response_id' => 'Response ID',
            'driver_id' => 'Driver ID',
            'survey_id' => 'Survey ID',
            'comment' => 'Comment',
            'photos' => 'Photos',
            'videos' => 'Videos',
            'points' => 'Points',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Drivers::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurvey()
    {
        return $this->hasOne(Surveys::className(), ['survey_id' => 'survey_id']);
    }
}
