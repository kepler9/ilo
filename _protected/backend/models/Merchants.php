<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "merchants".
 *
 * @property int $merchant_id
 * @property int $user_id
 * @property string $company
 * @property string $logo
 * @property string $location
 * @property string $latitude
 * @property string $longitude
 * @property double $rating
 * @property int $status
 *
 * @property Accessories[] $accessories
 * @property Ads[] $ads
 * @property FuelUsage[] $fuelUsages
 * @property Users $user
 * @property Services[] $services
 * @property Surveys[] $surveys
 */
class Merchants extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'merchants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'company'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['rating'], 'number'],
            [['company', 'logo', 'location', 'latitude', 'longitude'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'merchant_id' => 'Merchant ID',
            'user_id' => 'User ID',
            'company' => 'Company',
            'logo' => 'Logo',
            'location' => 'Location',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'rating' => 'Rating',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessories()
    {
        return $this->hasMany(Accessories::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFuelUsages()
    {
        return $this->hasMany(FuelUsage::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Services::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveys()
    {
        return $this->hasMany(Surveys::className(), ['merchant_id' => 'merchant_id']);
    }
}
