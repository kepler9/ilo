<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "surveys".
 *
 * @property int $survey_id
 * @property string $name
 * @property string $description
 * @property string $photo
 * @property int $merchant_id
 * @property int $status
 * @property int $points_weight
 * @property string $date_created
 *
 * @property Responses[] $responses
 * @property Merchants $merchant
 */
class Surveys extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'surveys';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'merchant_id'], 'required'],
            [['description'], 'string'],
            [['merchant_id', 'status', 'points_weight'], 'integer'],
            [['date_created'], 'safe'],
            [['name', 'photo'], 'string', 'max' => 255],
            [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Merchants::className(), 'targetAttribute' => ['merchant_id' => 'merchant_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'survey_id' => 'Survey ID',
            'name' => 'Name',
            'description' => 'Description',
            'photo' => 'Photo',
            'merchant_id' => 'Merchant ID',
            'status' => 'Status',
            'points_weight' => 'Points Weight',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponses()
    {
        return $this->hasMany(Responses::className(), ['survey_id' => 'survey_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchants::className(), ['merchant_id' => 'merchant_id']);
    }
}
