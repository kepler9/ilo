<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "templates".
 *
 * @property int $template_id
 * @property string $name
 * @property string $subject
 * @property string $body
 * @property string $date_added
 */
class Templates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'subject', 'body'], 'required'],
            [['body'], 'string'],
            [['date_added'], 'safe'],
            [['name', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'template_id' => 'Template ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'body' => 'Body',
            'date_added' => 'Date Added',
        ];
    }
}
