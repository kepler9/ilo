<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mail_queue".
 *
 * @property int $id
 * @property string $sender_name
 * @property string $sender_email
 * @property string $email_to
 * @property string $cc
 * @property string $bcc
 * @property string $subject
 * @property string $html_body
 * @property string $text_body
 * @property string $reply_to
 * @property string $charset
 * @property string $created_at
 * @property int $attempts
 * @property string $last_attempt_time
 * @property string $sent_time
 * @property string $time_to_send
 * @property string $swift_message
 * @property string $attachments
 * @property int $sent
 * @property string $email_template
 */
class MailQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mail_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender_name', 'sender_email', 'email_to', 'cc', 'bcc', 'html_body', 'text_body', 'reply_to', 'swift_message', 'attachments'], 'string'],
            [['created_at', 'last_attempt_time', 'sent_time', 'time_to_send'], 'safe'],
            [['attempts', 'sent'], 'integer'],
            [['subject', 'charset', 'email_template'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_name' => 'Sender Name',
            'sender_email' => 'Sender Email',
            'email_to' => 'Email To',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'subject' => 'Subject',
            'html_body' => 'Html Body',
            'text_body' => 'Text Body',
            'reply_to' => 'Reply To',
            'charset' => 'Charset',
            'created_at' => 'Created At',
            'attempts' => 'Attempts',
            'last_attempt_time' => 'Last Attempt Time',
            'sent_time' => 'Sent Time',
            'time_to_send' => 'Time To Send',
            'swift_message' => 'Swift Message',
            'attachments' => 'Attachments',
            'sent' => 'Sent',
            'email_template' => 'Email Template',
        ];
    }
}
