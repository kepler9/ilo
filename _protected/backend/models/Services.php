<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "services".
 *
 * @property int $service_id
 * @property int $service_type_id
 * @property string $name
 * @property string $description
 * @property int $merchant_id
 * @property double $price
 * @property string $image
 * @property string $location
 * @property string $latitude
 * @property string $longitude
 * @property int $status
 * @property string $date_created
 *
 * @property ServiceTypes $serviceType
 * @property Merchants $merchant
 * @property ServicesOrders[] $servicesOrders
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_type_id', 'name', 'merchant_id', 'location', 'status'], 'required'],
            [['service_type_id', 'merchant_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['date_created'], 'safe'],
            [['name', 'image', 'location', 'latitude', 'longitude'], 'string', 'max' => 255],
            [['service_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceTypes::className(), 'targetAttribute' => ['service_type_id' => 'service_type_id']],
            [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Merchants::className(), 'targetAttribute' => ['merchant_id' => 'merchant_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'service_type_id' => 'Service Type ID',
            'name' => 'Name',
            'description' => 'Description',
            'merchant_id' => 'Merchant ID',
            'price' => 'Price',
            'image' => 'Image',
            'location' => 'Location',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(ServiceTypes::className(), ['service_type_id' => 'service_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchants::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesOrders()
    {
        return $this->hasMany(ServicesOrders::className(), ['service_id' => 'service_id']);
    }
}
