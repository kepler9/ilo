<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "drivers".
 *
 * @property int $driver_id
 * @property int $user_id
 * @property int $loyalty_points
 * @property string $car_type
 * @property string $car_model
 * @property string $plate_number
 * @property string $car_color
 * @property double $monthly_fuel_budget
 * @property double $servicing_mileage Reminder Settings
 * @property string $insurance_renewal_date Reminder Settings
 * @property string $tyre_rotation_date Reminder Settings
 * @property string $wheel_balancing_alignment_date Reminder Settings
 * @property string $psv_ntsa_inspection Reminder Settings
 * @property string $driving_license_renewal_date Reminder Settings
 * @property int $status
 *
 * @property AccessoriesOrders[] $accessoriesOrders
 * @property Users $user
 * @property FuelUsage[] $fuelUsages
 * @property Redeemed[] $redeemeds
 * @property Responses[] $responses
 * @property ServicesOrders[] $servicesOrders
 */
class Drivers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'loyalty_points', 'status'], 'integer'],
            [['monthly_fuel_budget', 'servicing_mileage'], 'number'],
            [['insurance_renewal_date', 'tyre_rotation_date', 'wheel_balancing_alignment_date', 'psv_ntsa_inspection', 'driving_license_renewal_date'], 'safe'],
            [['car_type', 'car_model', 'plate_number', 'car_color'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'driver_id' => 'Driver ID',
            'user_id' => 'User ID',
            'loyalty_points' => 'Loyalty Points',
            'car_type' => 'Car Type',
            'car_model' => 'Car Model',
            'plate_number' => 'Plate Number',
            'car_color' => 'Car Color',
            'monthly_fuel_budget' => 'Monthly Fuel Budget',
            'servicing_mileage' => 'Servicing Mileage',
            'insurance_renewal_date' => 'Insurance Renewal Date',
            'tyre_rotation_date' => 'Tyre Rotation Date',
            'wheel_balancing_alignment_date' => 'Wheel Balancing Alignment Date',
            'psv_ntsa_inspection' => 'Psv Ntsa Inspection',
            'driving_license_renewal_date' => 'Driving License Renewal Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessoriesOrders()
    {
        return $this->hasMany(AccessoriesOrders::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFuelUsages()
    {
        return $this->hasMany(FuelUsage::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRedeemeds()
    {
        return $this->hasMany(Redeemed::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponses()
    {
        return $this->hasMany(Responses::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesOrders()
    {
        return $this->hasMany(ServicesOrders::className(), ['driver_id' => 'driver_id']);
    }
}
