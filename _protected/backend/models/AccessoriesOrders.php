<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "accessories_orders".
 *
 * @property int $id
 * @property int $accessory_id
 * @property int $driver_id
 * @property double $amount
 * @property int $quantity
 * @property string $odometer
 * @property string $location
 * @property string $status
 * @property string $date_ordered
 *
 * @property Drivers $driver
 * @property Accessories $accessory
 */
class AccessoriesOrders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accessories_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['accessory_id', 'driver_id', 'amount'], 'required'],
            [['accessory_id', 'driver_id', 'quantity'], 'integer'],
            [['amount'], 'number'],
            [['date_ordered'], 'safe'],
            [['odometer', 'location', 'status'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drivers::className(), 'targetAttribute' => ['driver_id' => 'driver_id']],
            [['accessory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accessories::className(), 'targetAttribute' => ['accessory_id' => 'accessory_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'accessory_id' => 'Accessory ID',
            'driver_id' => 'Driver ID',
            'amount' => 'Amount',
            'quantity' => 'Quantity',
            'odometer' => 'Odometer',
            'location' => 'Location',
            'status' => 'Status',
            'date_ordered' => 'Date Ordered',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Drivers::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessory()
    {
        return $this->hasOne(Accessories::className(), ['accessory_id' => 'accessory_id']);
    }
}
