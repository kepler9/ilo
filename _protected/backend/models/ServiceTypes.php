<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "service_types".
 *
 * @property int $service_type_id
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property double $commission
 * @property string $date_created
 *
 * @property Services[] $services
 */
class ServiceTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'commission'], 'required'],
            [['description'], 'string'],
            [['commission'], 'number'],
            [['date_created'], 'safe'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'service_type_id' => 'Service Type ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'description' => 'Description',
            'commission' => 'Commission',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Services::className(), ['service_type_id' => 'service_type_id']);
    }
}
