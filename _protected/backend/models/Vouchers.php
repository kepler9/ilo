<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vouchers".
 *
 * @property int $voucher_id
 * @property string $name
 * @property string $description
 * @property int $points
 * @property int $status
 * @property string $date_created
 *
 * @property Redeemed[] $redeemeds
 */
class Vouchers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vouchers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['points', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'voucher_id' => 'Voucher ID',
            'name' => 'Name',
            'description' => 'Description',
            'points' => 'Points',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRedeemeds()
    {
        return $this->hasMany(Redeemed::className(), ['voucher_id' => 'voucher_id']);
    }
}
