<?php
namespace backend\models;
use yii;
class Cron extends Models {
    public function send_emails() {
        $queue = MailQueue::find()->where(['sent' => 0])->limit(100)->all();
        if($queue){
            foreach($queue as $key){
                $key->email_to = str_replace(' ', '', $key->email_to);
                $key->email_to = strtolower($key->email_to);
                $key->bcc = ($key->bcc) ? $key->bcc : Yii::$app->params['bcc_email'];
                if (filter_var($key->email_to, FILTER_VALIDATE_EMAIL)) {
                    $email = Yii::$app->mailer->compose($key->email_template , [
                        'key' => $key,
                    ]);
                    $email->setFrom([$key->sender_email => $key->sender_name]);
                    if (filter_var($key->bcc, FILTER_VALIDATE_EMAIL)) {
                        $email->setBcc($key->bcc);
                    }
                    if (filter_var($key->reply_to, FILTER_VALIDATE_EMAIL)) {
                        $email->setReplyTo($key->reply_to);
                    }
                    if (filter_var($key->cc, FILTER_VALIDATE_EMAIL)) {
                        $email->setCc($key->cc);
                    }
                    $email->setTo($key->email_to);
                    $email->setSubject($key->subject);
                    if($key->attachments){
                        $attachments = json_decode($key->attachments,true);
                        foreach($attachments as $attachment){
                            $email->attach(dirname(dirname(dirname(__DIR__))).'/uploads/'.$attachment);
                        }
                    }
                    if($email->send()) {
                        $key->delete();
                    }else{
                        echo "Not Sent to ".$key->email_to; 
                        exit;
                    }
                }else{
                    echo "Invalid Email Address: $key->email_to"; 
                    exit;
                }
            }
        }
    }
    public function _send_sms(){
        $sms = SmsQueue::find()->all();
        $model = new Models();
        if($sms){
            foreach($sms as $key){
                $sent = $model->send_sms(['phone_number'=>$key->phone_number,'message'=>$key->message]);
                if($sent){
                    $key->delete();
                }
            }
        }
    }
}
