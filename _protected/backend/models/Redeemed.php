<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "redeemed".
 *
 * @property int $id
 * @property int $voucher_id
 * @property int $driver_id
 * @property int $status
 * @property string $date_created
 *
 * @property Contributors $contributor
 * @property Vouchers $voucher
 */
class Redeemed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'redeemed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['voucher_id', 'driver_id'], 'required'],
            [['voucher_id', 'driver_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contributors::className(), 'targetAttribute' => ['driver_id' => 'driver_id']],
            [['voucher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vouchers::className(), 'targetAttribute' => ['voucher_id' => 'voucher_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'voucher_id' => 'Voucher ID',
            'driver_id' => 'Contributor ID',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContributor()
    {
        return $this->hasOne(Contributors::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoucher()
    {
        return $this->hasOne(Vouchers::className(), ['voucher_id' => 'voucher_id']);
    }
}
