<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ads".
 *
 * @property int $ad_id
 * @property int $merchant_id
 * @property int $service_type_id
 * @property int $category_id
 * @property string $banner
 * @property string $name
 * @property int $clicks
 * @property string $link
 * @property int $status
 * @property string $date_created
 *
 * @property Merchants $merchant
 * @property ServiceTypes $serviceType
 * @property Categories $category
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['merchant_id', 'name'], 'required'],
            [['merchant_id', 'service_type_id', 'category_id', 'clicks', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['banner', 'name', 'link'], 'string', 'max' => 255],
            [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Merchants::className(), 'targetAttribute' => ['merchant_id' => 'merchant_id']],
            [['service_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceTypes::className(), 'targetAttribute' => ['service_type_id' => 'service_type_id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ad_id' => 'Ad ID',
            'merchant_id' => 'Merchant ID',
            'service_type_id' => 'Service Type ID',
            'category_id' => 'Category ID',
            'banner' => 'Banner',
            'name' => 'Name',
            'clicks' => 'Clicks',
            'link' => 'Link',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchants::className(), ['merchant_id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(ServiceTypes::className(), ['service_type_id' => 'service_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id']);
    }
}
