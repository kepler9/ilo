<?php
namespace backend\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Responses;
use yii;
/**
 * ResponsesSearch represents the model behind the search form of `backend\models\Responses`.
 */
class ResponsesSearch extends Responses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['response_id', 'driver_id', 'survey_id', 'points'], 'integer'],
            [['comment', 'photos', 'videos', 'date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Responses::find();
        $pagesize = Yii::$app->request->get('pagesize');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                // this $params['pagesize'] is an id of dropdown list that we set in view file
                'pagesize' => (isset($pagesize) ? $pagesize :  '20'),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'response_id' => $this->response_id,
            'driver_id' => Yii::$app->request->get('driver_id'),
            'survey_id' => Yii::$app->request->get('survey_id'),
            'points' => $this->points,
            'date_created' => $this->date_created,
        ]);
        $query->orderBy('response_id desc');
        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'videos', $this->videos]);

        return $dataProvider;
    }
}
