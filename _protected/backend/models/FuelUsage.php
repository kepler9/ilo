<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "fuel_usage".
 *
 * @property int $fuel_usage_id
 * @property int $driver_id
 * @property double $current_mileage
 * @property double $fuel_amount
 * @property double $fuel_unit_price
 * @property double $fuel_cost
 * @property string $fuel_type
 * @property string $location
 * @property int $merchant_id
 * @property string $date_posted
 *
 * @property Drivers $driver
 * @property Merchants $merchant
 */
class FuelUsage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fuel_usage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['driver_id', 'current_mileage', 'fuel_amount', 'fuel_unit_price', 'fuel_cost', 'fuel_type', 'location', 'merchant_id'], 'required'],
            [['driver_id', 'merchant_id'], 'integer'],
            [['current_mileage', 'fuel_amount', 'fuel_unit_price', 'fuel_cost'], 'number'],
            [['date_posted'], 'safe'],
            [['fuel_type', 'location'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drivers::className(), 'targetAttribute' => ['driver_id' => 'driver_id']],
            [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Merchants::className(), 'targetAttribute' => ['merchant_id' => 'merchant_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fuel_usage_id' => 'Fuel Usage ID',
            'driver_id' => 'Driver ID',
            'current_mileage' => 'Current Mileage',
            'fuel_amount' => 'Fuel Amount',
            'fuel_unit_price' => 'Fuel Unit Price',
            'fuel_cost' => 'Fuel Cost',
            'fuel_type' => 'Fuel Type',
            'location' => 'Location',
            'merchant_id' => 'Merchant ID',
            'date_posted' => 'Date Posted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Drivers::className(), ['driver_id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchants::className(), ['merchant_id' => 'merchant_id']);
    }
}
