<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Drivers;

/**
 * DriversSearch represents the model behind the search form of `backend\models\Drivers`.
 */
class DriversSearch extends Drivers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['driver_id', 'user_id', 'loyalty_points', 'status'], 'integer'],
            [['car_type', 'car_model', 'plate_number', 'car_color', 'insurance_renewal_date', 'tyre_rotation_date', 'wheel_balancing_alignment_date', 'psv_ntsa_inspection', 'driving_license_renewal_date'], 'safe'],
            [['monthly_fuel_budget', 'servicing_mileage'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Drivers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'driver_id' => $this->driver_id,
            'user_id' => $this->user_id,
            'loyalty_points' => $this->loyalty_points,
            'monthly_fuel_budget' => $this->monthly_fuel_budget,
            'servicing_mileage' => $this->servicing_mileage,
            'insurance_renewal_date' => $this->insurance_renewal_date,
            'tyre_rotation_date' => $this->tyre_rotation_date,
            'wheel_balancing_alignment_date' => $this->wheel_balancing_alignment_date,
            'psv_ntsa_inspection' => $this->psv_ntsa_inspection,
            'driving_license_renewal_date' => $this->driving_license_renewal_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'car_type', $this->car_type])
            ->andFilterWhere(['like', 'car_model', $this->car_model])
            ->andFilterWhere(['like', 'plate_number', $this->plate_number])
            ->andFilterWhere(['like', 'car_color', $this->car_color]);

        return $dataProvider;
    }
}
