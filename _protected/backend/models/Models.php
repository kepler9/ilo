<?php
namespace backend\models;
use yii;
/**
** 
**/
class Models extends \yii\base\Model {
    public function send_sms($data) {
        if(Yii::$app->params['sms_enabled']) {
            $variables = Variables::find()->one();
            if($variables && $variables->sms_balance >= 1.6){
                require_once  dirname(dirname(__DIR__)). '/vendor/africastalking/AfricasTalkingGateway.php';
                $gateway = new \AfricasTalkingGateway(Yii::$app->params['at_username'],Yii::$app->params['at_apikey']);
                try {
                    $phone_number = substr($data['phone_number'], -9);
                    if(strlen($phone_number) == 9){
                        $data['sender_id'] = (!empty($data['sender_id'])) ? $data['sender_id'] : Yii::$app->params['sender_id'];
                        $response = $gateway->sendMessage("+254".$phone_number,$data['message'],$data['sender_id']);
                        return true;
                    }
                } catch(AfricasTalkingGatewayException $e) {
                    return json_encode(['error'=>1,'message'=>'A problem was encountered while sending: '.$e->getMessage()]);
                }
            }else{
                $queue = new MailQueue;
                $queue->email_to = Yii::$app->params['adminEmail'];
                $queue->subject = 'Insufficient Balance';
                $queue->html_body = '<p>Insufficient balance while sending SMS. Available Balance: KES '.$variables->sms_balance.'</p>';
                $queue->html_body .= '<p>Top up now via Mpesa</p><p>Paybill: '.Yii::$app->params['at_sms_paybill'].'</p><p>Account No: '.Yii::$app->params['at_sms_account_no'].'</p>';
                if($queue->save()){
                    return json_encode(['error'=>1,'message'=>'Insufficient Balance']);
                }
            }
        } else {
            return json_encode(['error'=>1,'message'=>$data['message']]);
        }
    }
    public function update_variables() {
        $variables = ['sms_balance'];
        if($variables){
            foreach($variables as $variable){
                switch ($variable) {
                    case 'sms_balance':
                        require_once  dirname(dirname(__DIR__)). '/vendor/africastalking/AfricasTalkingGateway.php';
                        $gateway = new \AfricasTalkingGateway(Yii::$app->params['at_username'],Yii::$app->params['at_apikey']);
                        $data = $gateway->getUserData();
                        $balance = str_replace('KES ', '', $data->balance);
                        $balance = (double) str_replace('KES ', '', $data->balance);
                        $variables = Variables::find()->one();
                        $variables->sms_balance = $balance;
                        $variables->save();
                        break;
                    default:
                        break;
                }
            }
        }
    }
	public function time_elapsed($date) {
        $time = strtotime($date);
        if(time() > $time){
            $time = time() - $time;
            $period = 'ago';
        }else{
            $time = $time - time();
            $period = 'left';
        }
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        foreach($tokens as $unit => $text){
            if($time < $unit) continue;
            $units = floor($time/$unit);
            $append = ($units>1) ? 's' : '';
            return $units . ' ' . $text . $append.' '.$period;
        }
        return '0 seconds '.$period;
    }
   public function error_msg($msg=null) {
        Yii::$app->session->setFlash('error_msg',true);
        if($msg){
            Yii::$app->session->setFlash('msg',$msg);
        }
    }
    public function msg($msg=null) {
        if($msg){
            Yii::$app->session->setFlash('msg',$msg);
        }
    }
    public function clean($string) {
        $string = str_replace(' ', '-', $string);
        return strtolower(preg_replace('/[^A-Za-z0-9\-.]/', '-', $string));
    }
    public function get_model_errors($model){
        $str = "";
        foreach($model->getErrors() as $error){
            $str .= $error[0]." ";
        }
        return rtrim($str,' ');
    }
    public function upload_file($field,$target_dir='../uploads/'){
        if(Yii::$app->request->post()){
            if(isset($_FILES[$field]['name'])){
                $file_name = basename($_FILES[$field]["name"]);
                if(move_uploaded_file($_FILES[$field]['tmp_name'], $target_dir.$file_name)) {
                  return $file_name;
                }
            }
        }
    }
    public function upload_files($field,$target_dir = "../uploads/"){
        if(Yii::$app->request->post()){
            if(isset($_FILES[$field]['name'])){
                $files = false;
                if(!empty($_FILES[$field]['name'])){
                   $count = count($_FILES[$field]['name']);
                    if($count){
                        $files = array();
                        for($i=0;$i<$count;$i++){
                            $file_name = basename($_FILES[$field]["name"][$i]);
                            if(move_uploaded_file($_FILES[$field]['tmp_name'][$i], $target_dir.$file_name)) {
                              $files[] = $file_name;
                            }
                        }
                    } 
                }
                return $files;
            }
        }
    }
    public function format_phone($phone){
        $phone = str_replace(' ', '', $phone);
        return substr($phone, -9);
    }
    public function summary_statistics(){
    	$data['merchants'] = Merchants::find()->count();
    	$data['services'] = Services::find()->count();
    	$data['drivers'] = Drivers::find()->count();
    	$data['orders'] = ServicesOrders::find()->count() + AccessoriesOrders::find()->count();
    	return (object) $data;
    }
    public function app_signin($data){
        $user = Users::find()->where(['email'=>$data['email'],'role'=>'driver'])->one();
        if($user){
            $driver = Drivers::find()->where(['user_id'=>$user['user_id']])->one();
            $user->load(['Users'=>$data]);
            if(isset($data['password'])){
                $user->password = Yii::$app->security->generatePasswordHash($data['password']);
            }
            $driver->load(['Drivers'=>$data]);
            $user->save();
            $driver->save();
            #fetch data as arrays
            $_user = Users::find()->where(['user_id'=>$user->user_id])->select('user_id,first_name,last_name,email,phone,status,date_created')->asArray()->one();
            $_driver = Drivers::find()->where(['driver_id'=>$driver->driver_id])->select('loyalty_points,car_type,car_model,plate_number,
                car_color,monthly_fuel_budget,servicing_mileage,insurance_renewal_date,tyre_rotation_date,
                wheel_balancing_alignment_date,psv_ntsa_inspection,driving_license_renewal_date')->asArray()->one();
            $_user['full_name'] = $_user['first_name'].' '.$_user['last_name'];
            unset($_user['first_name']);
            unset($_user['last_name']);
            return array_merge($_user,$_driver);
        }else{
            return $this->app_signup($data);
        }
    }
    public function app_signup($data){
        if(isset($data['full_name']) && isset($data['email'])){
            $user = new Users();
            $user->load(['Users'=>$data]);
            $user->email = $data['email'];
            $full_name = trim($data['full_name']);
            $parts = explode(' ', $full_name);
            if($parts){
                if(count($parts) > 1){
                    $user->first_name = $parts[0];
                    $user->last_name = $parts[1];
                }else{
                    $user->first_name = $full_name;
                }
            }else{
               $user->first_name = $full_name; 
            }
            if(isset($data['password'])){
                $user->password = Yii::$app->security->generatePasswordHash($data['password']);
            }
            if($user->save()){
                $driver = new Drivers();
                $driver->load(['Drivers'=>$data]);
                $driver->user_id = $user->user_id;
                if($driver->save()){
                    return $this->app_signin(['email'=>$user->email]);
                }else{
                    return $driver->getErrors();
                }
            }else{
                return $user->getErrors();
            } 
        }
        return ['error'=>true,'message'=>'failed to create account'];
    }
    public function app_service_providers(){
        $merchants = Merchants::find()->where(['status'=>1])->all();
        if($merchants){
            foreach($merchants as $merchant){
                $list[] = [
                    'merchant_id' => $merchant->merchant_id,
                    'company' => $merchant->company,
                    'location' => $merchant->location,
                    'latitude' => $merchant->latitude,
                    'longitude' => $merchant->longitude,
                    'rating' => $merchant->rating,
                    'logo' => Yii::$app->request->hostInfo.'/uploads/'.$merchant->logo,
                    'first_name' => $merchant->user->first_name,
                    'last_name' => $merchant->user->last_name,
                    'email' => $merchant->user->email,
                    'phone' => $merchant->user->phone,
                    'date_created' => $merchant->user->date_created
                ];
            }
            return $list;
        }
        return ['error'=>true,'message'=>'failed to get service providers'];
    }
    public function app_service_provider($merchant_id){
        if($merchant_id){
            $merchant = Merchants::find()->where(['merchant_id'=>$merchant_id,'status'=>1])->select('user_id,merchant_id,company,location,latitude,longitude,rating,logo')->asArray()->one();
            if($merchant){
                $merchant['logo'] = Yii::$app->request->hostInfo.'/uploads/'.$merchant['logo'];
                $user = Users::find()->where(['user_id'=>$merchant['user_id']])->select('first_name,last_name,email,phone,date_created')->asArray()->one();
                unset($merchant['user_id']);
                return array_merge($merchant,$user);
            }
        }
        return ['error'=>true,'message'=>'service provider not found'];
    }
    public function app_save_fuel($data){
        if(isset($data['merchant_id']) && isset($data['email'])){
            $user = $this->app_user_by_email($data['email']);
            if($user){
                $model = new FuelUsage();
                $driver = Drivers::findOne(['user_id'=>$user->user_id]);
                if($driver){
                    $model->driver_id = $driver->driver_id;
                    $model->merchant_id = $data['merchant_id'];
                    $model->current_mileage = $data['odometer'];
                    $model->fuel_unit_price = $data['priceperliter'];
                    $model->fuel_cost = $data['totalcost'];
                    $model->fuel_amount = $data['litres'];
                    $model->fuel_type = $data['fueltype'];
                    $model->location = $data['location'];
                    $model->date_posted = $data['date'];
                    if($model->save()){
                        return array_merge(['status'=>'success'],FuelUsage::find()->where(['fuel_usage_id'=>$model->fuel_usage_id])->asArray()->select('fuel_usage_id,current_mileage,fuel_unit_price,fuel_cost,fuel_amount,fuel_type,location,date_posted')->one());
                    }else{
                        return $model->getErrors();
                    }
                }
            }
        }else{
            return ['error'=>true,'message'=>'merchant_id, email is required'];
        }
        return ['error'=>true,'message'=>'error saving fuel'];
    }
    public function app_fuel_history($email){
        if($email){
            $user = $this->app_user_by_email($email);
            if($user){
                $driver = Drivers::findOne(['user_id'=>$user->user_id]);
                if($driver){
                    return FuelUsage::find()->where(['driver_id'=>$driver->driver_id])->asArray()->select('merchant_id,current_mileage,fuel_unit_price,fuel_cost,fuel_amount,fuel_type,location,date_posted')->all();
                }
            }
        }
        return ['error'=>true,'message'=>'fuel history not found'];
    }
    public function app_accessories($email){
        $accessories = Accessories::find()->where(['status'=>1])->asArray()->select('accessory_id,name,description,price,photo,merchant_id,category_id')->all();
        if($accessories){
            foreach($accessories as $key){
                $key['photo'] = Yii::$app->request->hostInfo.'/uploads/'.$key['photo'];
                $category = Categories::findOne($key['category_id']);
                unset($key['category_id']);
                $key['category'] = $category->name;
                $list[] = $key;
            }
            return $list;
        }
        return ['error'=>true,'message'=>'accessories list not found'];
    }
    public function app_services($email){
        $services = Services::find()->where(['status'=>1])->asArray()->all();
        if($services){
            foreach($services as $key){
                $key['image'] = Yii::$app->request->hostInfo.'/uploads/'.$key['image'];
                $category = ServiceTypes::findOne($key['service_type_id']);
                unset($key['status']);
                unset($key['date_created']);
                $key['service_type'] = $category->name;
                $list[] = $key;
            }
            return $list;
        }
        return ['error'=>true,'message'=>'accessories list not found'];
    }
    public function app_order_service($data){
        if(isset($data['email'])){
            $user = $this->app_user_by_email($data['email']);
            if($user){
                $driver = Drivers::findOne(['user_id'=>$user->user_id]);
                if($driver){
                    $model = new ServicesOrders();
                    $model->load(['ServicesOrders'=>$data]);
                    $service = Services::findOne($model->service_id);
                    $model->driver_id = $driver->driver_id;
                    $model->amount = $service->price;
                    if($model->save()){
                        $model = ServicesOrders::findOne($model->id);
                        return [
                            'order_id'=>$model->id,
                            'service'=>$model->service->name,
                            'amount'=>$service->price,
                            'status'=>$model->status,
                            'date_ordered'=>$model->date_ordered,
                            'odometer'=>$model->odometer,
                            'location'=>$model->location,
                        ];
                    }else{
                        return $model->getErrors();
                    }
                }
            }
        }
        return ['error'=>true,'message'=>'failed to create order'];
    }
    public function app_order_accessory($data){
        if(isset($data['email'])){
            $user = $this->app_user_by_email($data['email']);
            if($user){
                $driver = Drivers::findOne(['user_id'=>$user->user_id]);
                if($driver){
                    $model = new AccessoriesOrders();
                    $model->load(['AccessoriesOrders'=>$data]);
                    $accessory = Accessories::findOne($model->accessory_id);
                    if($accessory){
                        $model->driver_id = $driver->driver_id;
                        $model->amount = $model->quantity*$accessory->price;
                        if($model->save()){
                            $model = AccessoriesOrders::findOne($model->id);
                            return [
                                'order_id'=>$model->id,
                                'accessory'=>$model->accessory->name,
                                'quantity'=>$model->quantity,
                                'amount'=>$model->amount,
                                'status'=>$model->status,
                                'date_ordered'=>$model->date_ordered,
                                'odometer'=>$model->odometer,
                                'location'=>$model->location,
                            ];
                        }else{
                            return $model->getErrors();
                        }
                    }
                }
            }
        }
        return ['error'=>true,'message'=>'failed to create order'];
    }
    public function app_service_requests($email){
        $user = $this->app_user_by_email($email);
        if($user){
            $driver = Drivers::findOne(['user_id'=>$user->user_id]);
            if($driver){
                $models = ServicesOrders::find()->where(['driver_id'=>$driver->driver_id])->asArray()->all();
                if($models){
                    foreach($models as $model){
                        $service = Services::findOne($model['service_id']);
                        $model['service'] = $service->name;
                        unset($model['driver_id']);
                        $model['order_id'] = $model['id'];
                        unset($model['id']);
                        $list[] = $model;
                    }
                    return $list;
                }
            }
        }
        return ['error'=>true,'message'=>'service requests not found'];
    }
    public function app_accessory_requests($email){
        $user = $this->app_user_by_email($email);
        if($user){
            $driver = Drivers::findOne(['user_id'=>$user->user_id]);
            if($driver){
                $models = AccessoriesOrders::find()->where(['driver_id'=>$driver->driver_id])->asArray()->all();
                if($models){
                    foreach($models as $model){
                        $accessory = Accessories::findOne($model['accessory_id']);
                        $model['accessory'] = $accessory->name;
                        unset($model['driver_id']);
                        $model['order_id'] = $model['id'];
                        unset($model['id']);
                        $list[] = $model;
                    }
                    return $list;
                }
            }
        }
        return ['error'=>true,'message'=>'accessory requests not found'];
    }
    public function app_user_auth($data){
        if(isset($data['email']) && isset($data['password'])){
            $user = Users::findOne(['email'=>$data['email']]);
            if($user){
                if(Yii::$app->security->validatePasswordHash($user->password,$data['password'])){
                    return $this->app_signin($data);
                }else{
                    return ['error'=>true,'message'=>'wrong password'];
                }
            }else{
                return ['error'=>true,'message'=>'wrong email'];
            }
        }else{
            return ['error'=>true,'message'=>'email, password is required'];
        }
    }
    public function app_user_by_email($email){
        return Users::findOne(['email'=>$email,'role'=>'driver']);
    }
    public function app_summary($email){
        $user = $this->app_user_by_email($email);
        $driver = Drivers::findOne(['user_id'=>$user->user_id]);
        $latest_service = ServicesOrders::find()->where(['driver_id'=>$driver->driver_id])->orderBy('id desc')->one();
        $latest_accessory = AccessoriesOrders::find()->where(['driver_id'=>$driver->driver_id])->orderBy('id desc')->one();
        $latest_fueling = FuelUsage::find()->where(['driver_id'=>$driver->driver_id])->orderBy('fuel_usage_id desc')->one();
        return [
            ['services'=>[
                'count' => ServicesOrders::find()->where(['driver_id'=>$driver->driver_id])->count(),
                'amount' => ServicesOrders::find()->where(['driver_id'=>$driver->driver_id])->sum('amount') ?? 0,
                'last_date' =>  $latest_service ? $latest_service->date_ordered : "",
            ]],
            ['accessories'=>[
                'count' => AccessoriesOrders::find()->where(['driver_id'=>$driver->driver_id])->count(),
                'amount' => AccessoriesOrders::find()->where(['driver_id'=>$driver->driver_id])->sum('amount') ?? 0,
                'last_date' => $latest_accessory ? $latest_accessory->date_ordered : "",
            ]],
            ['fuel'=>[
                'litres' => FuelUsage::find()->where(['driver_id'=>$driver->driver_id])->sum('fuel_amount') ?? 0,
                'cost' => FuelUsage::find()->where(['driver_id'=>$driver->driver_id])->sum('fuel_cost') ?? 0,
                'last_date' => $latest_fueling ? $latest_fueling->date_posted : "",
            ]],
        ];
    }
}