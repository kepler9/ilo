<?php
namespace frontend\controllers;
use yii;
use backend\models\Users;
use backend\models\Contributors;
use backend\models\MailQueue;
use backend\models\Templates;
/**
**
**/
class WelcomeController extends MainController {
   public function actionIndex(){
    return $this->redirect(['/dashboard/account/login']);
   }
   public function actionLogout() {
        Yii::$app->session->removeAll();
        return $this->redirect(['/']);
    }
    public function actionForgot(){
        $data = Yii::$app->request->post();
        if($data){
            $user = Users::findOne(['email'=>$data['email']]);
            if($user){
                $template = Templates::findOne(['name'=>'forgot_password']);
                if($template){
                    $password = 'ocular_'.rand(2000,3000);
                    $queue = new MailQueue();
                    $queue->email_to = $data['email'];
                    $queue->subject = $template->subject;
                    $queue->html_body = $template->body;
                    $queue->html_body = str_replace('[NAME]', $user->first_name, $queue->html_body);
                    $queue->html_body = str_replace('[PASSWORD]', $password, $queue->html_body);
                    $queue->html_body = str_replace('[LINK]', Yii::$app->request->hostInfo, $queue->html_body);
                    if($queue->save()){
                        $user->password = Yii::$app->getSecurity()->generatePasswordHash($password);
                        $user->save();
                        $this->model->msg('Password reset instructions sent, check your email.');
                    }
                }
            }else{
                $this->model->error_msg('Wrong Email');
            }
        }
        return $this->render('forgot',['model'=>new Users()]);
    }
}
