<?php
namespace frontend\controllers;
use yii\helpers\Json;
use yii;
use backend\models\Models;
use yii\validators\EmailValidator;
/**
** 
**/
class ApiController extends \yii\web\Controller
{
	public $model;
	public $enableCsrfValidation = false;
	public function beforeAction($action){
		$this->model = new Models();
		return parent::beforeAction($action);
	}
	public function actionIndex(){
		return Json::encode(['API_VERSION'=>2]);
	}
	public function actionAuth(){
		if(Yii::$app->request->isPost){
			$data = Yii::$app->request->queryParams;
			$response = $this->model->app_user_auth($data);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected POST method']);
		}
	}
	public function actionSignin(){
		if(Yii::$app->request->isPost){
			$data = Yii::$app->request->queryParams;
			if(isset($data['email'])){
				$validator = new EmailValidator();
				if($validator->validate($data['email'])){
					$response = $this->model->app_signin($data);
					return Json::encode($response);
				}else{
					return Json::encode(['error'=>true,'message'=>'email is invalid']);
				}
			}else{
				return Json::encode(['error'=>true,'message'=>'email is required']);
			}
		}else{
			return Json::encode(['error'=>true,'message'=>'expected POST method']);
		}
	}
	public function actionServiceProviders(){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_service_providers();
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionServiceProvider($merchant_id){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_service_provider($merchant_id);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionSaveFuel(){
		if(Yii::$app->request->isPost){
			$data = Yii::$app->request->queryParams;
			$response = $this->model->app_save_fuel($data);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected POST method']);
		}
	}
	public function actionFuelHistory($email){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_fuel_history($email);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionAccessories($email=null){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_accessories($email);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionServices($email=null){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_services($email);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionSaveService(){
		if(Yii::$app->request->isPost){
			$data = Yii::$app->request->queryParams;
			$response = $this->model->app_order_service($data);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected POST method']);
		}
	}
	public function actionSaveAccessory(){
		if(Yii::$app->request->isPost){
			$data = Yii::$app->request->queryParams;
			$response = $this->model->app_order_accessory($data);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected POST method']);
		}
	}
	public function actionServiceRequests($email){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_service_requests($email);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionAccessoryRequests($email){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_accessory_requests($email);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
	public function actionSummary($email){
		if(Yii::$app->request->isGet){
			$response = $this->model->app_summary($email);
			return Json::encode($response);
		}else{
			return Json::encode(['error'=>true,'message'=>'expected GET method']);
		}
	}
}