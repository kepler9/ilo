<?php
namespace frontend\controllers;
use yii;
use backend\models\Models;
use backend\models\Users;
/**
**
**/
class MainController extends \yii\web\Controller
{
   public $model;
   public $user;
   public $contributor;
   public function beforeAction($action){
    $this->layout = 'frontend';
    $this->model = new Models();
    if(Yii::$app->session->get('user')){
    	$this->user = Users::findOne(Yii::$app->session->get('user')->user_id);
    	if($this->user->role == 'contributor'){
    		$this->contributor = $this->user->contributors[0];
    	}
    }
    return parent::beforeAction($action);
   }
}
