<?php 
	$this->title = "My Account";
	use backend\models\SquareContributors;
	$count = $squares ? count($squares) : 0;
	use backend\models\Responses;
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="cards-header">
				<center><img src="/uploads/ocular-dark.png" class="img-responsive"></center>
			</div>
		</div>
		<div class="cards">
			<div class="col-md-8 col-md-offset-2">
				<?=$this->render('//layouts/alert');?>
			</div>
			<span class="clearfix"></span>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
				<?php $count = 0; if($squares): foreach($squares as $key): ++$count;?>
					<div class="col-md-6 col-xs-12">
						<div class="boxed shadow square">
							<p><?=$key->name?></p>
							<div class="square-image">
								<center>
									<a href="/users/square?id=<?=$key->square_id?>">
										<img class="img-responsive" src="/uploads/<?=$key->photo ? $key->photo : 'square.png' ?>">
									</a>
								</center>
							</div>
							<div class="square-details">
								<?php $responses = Responses::find()->where(['square_id'=>$key->square_id])->count();?>
								<div class="divider"><hr></div>
								<p><?=substr($key->description,0,150)?><a href="/users/square?id=<?=$key->square_id?>"> ... read more</a></p>
								<div class="divider"><hr></div>
								<ul class="list-unstyled list-inline smaller-font">
									<li><i class="fa fa-clock-o"></i> <?=$this->context->model->time_elapsed($key->date_created);?> |</li>
									<li>Comments <sup><span class="badge badge-danger"><?=$responses?></span></sup></li>
								</ul>
								<br>
								<a href="/users/square?id=<?=$key->square_id?>" class="btn btn-orange pull-right">Take me to the activity</a>
								<span class="clearfix"></span>
							</div>
						</div>
						<br>
					</div>
					<?php if($count % 2 == 0):?>
						<span class="clearfix"></span>
					<?php endif;?>
					<?php endforeach; else:?>
					<div class="boxed bordered-boxed">
						<center>No activity for you today</center>
					</div>
				<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>