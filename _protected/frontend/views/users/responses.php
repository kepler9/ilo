<h5>Comments <sup><span class="badge badge-danger"><?=count($responses);?></span></sup></h5><br>
<?php if($responses): foreach($responses as $key):?>
	<div class="row">
		<div class="col-md-1">
			<div class="profile-picture-sm">
				<img src="/uploads/<?=$key->contributor->user->photo ? $key->contributor->user->photo : 'avatar.png'?>">
			</div>
		</div>
		<div class="col-md-11">
			<p><b><?=$key->contributor->user->first_name?>: </b><?=$key->comment?></p>
			<ul class="list-unstyled list-inline pull-right smaller-font">
				<li><i><i class="fa fa-clock-o"></i> <?=$this->context->model->time_elapsed($key->date_created);?></i></li>
			</ul>
			<span class="clearfix"></span>
			<?php if($key->photos):?>
				<hr>
				<?php 
					$photos = json_decode($key->photos,true);
				?>
				<ul class="list-unstyled list-inline">
					<?php foreach($photos as $photo):?>
						<li><a target="_blank" href="/uploads/<?=$photo?>"><img src="/uploads/<?=$photo?>" width="50"></a></li>
					<?php endforeach;?>
				</ul>
			<?php endif;?>
			<?php if($key->videos):?>
				<hr>
				<?php 
					$videos = json_decode($key->videos,true);
				?>
				<div class="row">
					<?php foreach($videos as $video):?>
						<div class="col-md-4">
							<video class="video" autobuffer autoloop loop controls  width="400">
								<source src="/uploads/<?=$video?>">
								<source src="/uploads/<?=$video?>">
								<object type="video/mp4" data="/uploads/<?=$video?>">
								<param name="src" value="/uploads/<?=$video?>">
								<param name="autoplay" value="false">
								<param name="autoStart" value="0">
								<p><a href="/uploads/<?=$video?>">Download this video file.</a></p>
								</object>
							</video>
						</div>
					<?php endforeach;?>
				</div>
			<?php endif;?>
		</div>
	</div>
	<hr>
<?php endforeach;else:?>
	<p>Be the first to comment</p>
<?php endif;?>