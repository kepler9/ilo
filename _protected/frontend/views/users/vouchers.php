<?php 
$points = number_format($contributor->points);
$this->title = 'Redeem your '.$points.' points'; 
?>
<br><br>
<center>
<h4><?=$this->title?></h4>
</center>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<?=$this->render('//layouts/alert');?>
			<?php if($vouchers): foreach($vouchers as $key):?>
				<div class="boxed shadow">
					<div class="media-voucher">
						<div class="col-md-2">
							<img src="holder.js/100x100?text=<?=$key->points?>">
							<br><br>
						</div>
						<div class="col-md-10">
							<p><b><?=$key->name?></b></p>
							<p><?=$key->description?></p>
							<a href="/users/redeem?id=<?=$key->voucher_id?>" class="btn btn-theme">Redeem</a>
						</div>
					</div>
				</div>
				<br>
			<?php endforeach;endif;?>
		</div>
	</div>
</div>