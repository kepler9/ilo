<?php
	$this->title = $user->first_name;
	use yii\widgets\ActiveForm;
?>
<br><br>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="boxed shadow">
				<div class="profile-picture">
					<center><img src="/uploads/<?=$user->photo ? $user->photo : 'avatar.png'?>"></center>
					<br>
					<center><p><?=$user->first_name.' '.$user->last_name?></p></center>
				</div>
				<div class="divider"><hr></div>
				<p><b>Joined:</b> <?=date('F j, Y',strtotime($user->date_created));?></p>
				<p><b>Points:</b> <?=number_format($contributor->points)?></p>
				<p><a href="/users/vouchers">Redeem Points</a></p>
			</div>
		</div>
		<div class="col-md-9">
			<div class="boxed shadow">
				<?=$this->render('//layouts/alert');?>
				<?php $form = ActiveForm::begin();?>
					<?=$form->field($user,'first_name');?>
					<?=$form->field($user,'last_name');?>
					<?=$form->field($user,'email');?>
					<?=$form->field($user,'phone');?>
					<?=$form->field($user,'photo')->fileInput(['name'=>'photo'])->label(false);?>
					<div onclick="$('#users-photo').click()" class="btn btn-upload"><i class="fa fa-image"></i> Upload photo</div>
					<br>
					<div class="form-group pull-right">
						<button class="btn btn-theme">Save changes</button>
					</div>
					<span class="clearfix"></span>
				<?php ActiveForm::end();?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#users-photo').css('display', 'none');
</script>
