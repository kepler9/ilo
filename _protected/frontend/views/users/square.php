<?php
$this->title = $square->name;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
?>
<br><br><br>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="boxed shadow">
				<div class="square-image">
					<center><img class="img-responsive" src="/uploads/<?=$square->photo ? $square->photo : 'square.png' ?>"></center>
				</div>
				<div class="square-details">
					<p><?=$square->name?></p>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="boxed shadow">
				<p><?=$square->description?></p>
				<div class="divider"><hr></div>
				<div id="responses"></div>
				<br>
				<?=$this->render('//layouts/alert');?>
				<div class="comment-box">
					<?php $form = ActiveForm::begin(['action'=>'/users/comment?square_id='.$square->square_id]);?>
						<?=$form->field($response,'comment')->widget(CKEditor::className(),['editorOptions'=>['inline'=>false,'preset'=>'basic']])->label('Your comment');?>
						<div class="row">
							<div class="col-md-6">
								<?=$form->field($response,'photos')->fileInput(['name'=>'photos[]','multiple'=>'multiple','accept'=>'image/png,image/jpg'])->label(false);?>
								<div onclick="$('#responses-photos').click()" class="btn btn-upload"><i class="fa fa-image"></i> Upload photos</div>
							</div>
							<div class="col-md-6">
								<?=$form->field($response,'videos')->fileInput(['name'=>'videos[]','multiple'=>'multiple','accept'=>'video/mp4'])->label(false);?>
								<div onclick="$('#responses-videos').click()" class="btn btn-upload"><i class="fa fa-video-camera"></i> Upload videos&nbsp;</div>
							</div>
						</div>
						<br><hr>
						<div class="form-group pull-right">
							<button class="btn btn-theme">Submit</button>
						</div>
						<span class="clearfix"></span>
					<?php ActiveForm::end();?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$.get('/users/responses?id=<?=$square->square_id?>', function(data) {
			$('#responses').html(data);
		});
	});
	$('#responses-photos').css('display', 'none');
	$('#responses-videos').css('display', 'none');
</script>