<?php if(Yii::$app->session->getFlash('msg')):?>
	<div class="alert alert-<?=(Yii::$app->session->getFlash('error_msg')) ? 'danger' : 'success';?> alert-box">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
		<strong><?=Yii::$app->session->getFlash('msg');?></strong>
	</div>
<?php endif;?>