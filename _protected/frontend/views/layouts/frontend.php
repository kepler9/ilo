<?php
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" type="text/css" href="/themes/frontend/css/frontend.css?v=<?=time();?>">
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Square',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top shadow',
                ],
            ]);
            $menuItems[] = ['label' => Yii::t('app', 'Activities'), 'url' => ['/users/account']];
            if(Yii::$app->session->get('user')){
                if($this->context->user->role == 'contributor'){
                    $menuItems[] = ['label' => Yii::t('app', 'My Profile'), 'url' => ['/users/profile']];
                }else if($this->context->user->role == 'admin'){
                    $menuItems[] = ['label' => Yii::t('app', 'Dashboard'), 'url' => ['/dashboard']];
                }
                $menuItems[] = ['label' => Yii::t('app', 'Logout ('.$this->context->user->first_name.' '.$this->context->user->last_name.')'), 'url' => ['/welcome/logout']];
            }else{
                $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/']];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
