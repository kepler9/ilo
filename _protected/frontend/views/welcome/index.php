<?php 
$this->title = 'Ocular Agency - Best Infographics in Kenya, Motion Graphics, Interactive graphics '; 
use yii\widgets\ActiveForm;
?>
<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="boxed  shadow login-form">
			<div class="boxed-header">
				<center><img src="/uploads/ocular.png" class="img-responsive"></center>
			</div>
			<?=Yii::$app->controller->renderPartial('//layouts/alert');?>
			<?php $form = ActiveForm::begin(['action'=>'/users/login']);?>
				<?=$form->field($model,'email')->textInput(['placeholder'=>'Enter valid email address','name'=>'email']);?>
				<?=$form->field($model,'password')->textInput(['placeholder'=>'Enter your password','type'=>'password','name'=>'password']);?>
				<div class="form-group">
					<a href="/welcome/forgot">Forgot Password?</a>
				</div>
				<div class="form-group pull-right">
					<button class="btn btn-theme">Login</button>
				</div>
				<span class="clearfix"></span>
			<?php ActiveForm::end();?>
		</div>
	</div>
</div>