<?php 
$this->title = 'Ocular Agency - Best Infographics in Kenya, Motion Graphics, Interactive graphics '; 
use yii\widgets\ActiveForm;
?>
<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="boxed  shadow login-form">
			<div class="boxed-header">
				<center><img src="/uploads/ocular.png" class="img-responsive"></center>
			</div>
			<?=Yii::$app->controller->renderPartial('//layouts/alert');?>
			<?php $form = ActiveForm::begin(['action'=>'/welcome/forgot']);?>
				<p>Forgot password? No worries, enter your email for reset instructions.</p><br>
				<?=$form->field($model,'email')->textInput(['placeholder'=>'Enter valid email address','name'=>'email']);?>
				<div class="form-group pull-right">
					<button class="btn btn-theme">Submit</button>
				</div>
				<span class="clearfix"></span>
			<?php ActiveForm::end();?>
		</div>
	</div>
</div>